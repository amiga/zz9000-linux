#!/bin/bash

# for mkimage: apt install u-boot-tools
# compiler: apt install gcc-arm-linux-gnueabihf

git clone --depth 1 git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git

cp zz9000-kernel-config linux/.config

# softlink the driver code files into the kernel tree

ln -s $(pwd)/linux-changes/drivers/uio/uio_zz9000.c linux/drivers/uio/
ln -s $(pwd)/linux-changes/drivers/uio/zz9000_gfx.c linux/drivers/uio/
ln -s $(pwd)/linux-changes/drivers/uio/zz9000_gfx.h linux/drivers/uio/
ln -s $(pwd)/linux-changes/drivers/uio/zz9000_video_modes.h linux/drivers/uio/

cd linux

# patch Makefile and Kconfig

patch -p1 <../patches-kernel/mnt-zz9000-linux-uio.patch

# build uImage

make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- uImage LOADADDR=0x2000000 -j$(nproc)

