/*

Copyright (C) 2001-2003 Neil Cafferkey

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.

*/

#ifndef DEVICE_H
#define DEVICE_H


#include <exec/types.h>
#include <exec/devices.h>
#include <devices/serial.h>
#include <devices/timer.h>

#ifndef UPINT
typedef ULONG UPINT;
typedef LONG PINT;
#endif

#ifdef __mc68000
#define REG(A) __asm(A)
#else
#define REG(A)
#endif

//#define USE_HACKS


enum
{
   READ_QUEUE,
   WRITE_QUEUE,
   GENERAL_QUEUE,
   REQUEST_QUEUE_COUNT
};



struct DevUnit
{
   struct MinNode node;
   ULONG unit_num;
   ULONG open_count;
   ULONG flags;
   struct Task *task;
   struct MsgPort *request_ports[REQUEST_QUEUE_COUNT];
   struct DevBase *device;
   struct SignalSemaphore access_lock;
   volatile UBYTE *config_base;
   volatile UBYTE *io_base;
   struct IOStdReq *active_requests[2];
};


struct DevBase
{
  struct Device device;
  APTR seg_list;
  struct ExecBase *sys_base;
  struct UtilityBase *utility_base;
  struct DevUnit* unit;
  struct IOExtSer* pending_req;
  struct Process* task;
  struct Interrupt *db_int6;
  UBYTE lock;
};

/* Unit flags */

#define UNITF_SHARED (1<<0)
#define UNITF_7WIRE (1<<1)
#define UNITF_HAVECARD (1<<2)
#define UNITF_OVERRUN (1<<3)
#define UNITF_USEFIFOS (1<<4)

/* Request flags */

#define REQF_QUEUED (1<<1)

#define HANDLE_PRIORITY 10
#define ODD_OFFSET 0xffff

#define MIN_BAUD 110
#define MAX_BAUD 115200
#define DEFAULT_BAUD 9600
#define MIN_WORD_LENGTH 5
#define DEFAULT_BUFFER_SIZE 4096
#define DEFAULT_BREAK_TIME 250000

#define UART_IIR_CANUSEFIFOS (1<<6)   /* Only works when FCR bit 0 is set */

#endif
