/*

File: device.c
Author: Neil Cafferkey
Copyright (C) 2000-2003 Neil Cafferkey

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.

*/

#include <exec/types.h>
#include <exec/resident.h>
#include <exec/errors.h>
#include <exec/io.h>
#include "initializers.h"

#include <proto/exec.h>
#include <proto/alib.h>

#include "device.h"
//#include <devices/trackdisk.h>

#include "unit_protos.h"
#include "request_protos.h"

#define UNIT_COUNT 1

struct ExecBase* SysBase;

LONG Main()
{
   return -1;
}

const char DevName[]="zztty.device";
const char DevIdString[]="zztty 1.0 (06 Apr 2020)";
const UWORD DevVersion=1;
const UWORD DevRevision=0;

struct DevBase* _devbase;
void frame_proc();

#include "stabs.h"

int __UserDevInit(struct Device* dev)
{
  SysBase = *(struct ExecBase **)4L;

  _devbase = AllocMem(sizeof(struct DevBase), MEMF_PUBLIC|MEMF_CLEAR);
  if (!_devbase) return 0;

  // DevBase looks ok
  KPrintF("__UserDevInit DevBase: %lx\n", _devbase);
  
  return 1;
}

int __UserDevOpen(struct IOExtSer *ios, ULONG unitnum, ULONG flags)
{
  int io_err = IOERR_OPENFAIL;
  
  if (ios && unitnum==0 && !_devbase->unit) {
    _devbase->unit = CreateUnit(0, _devbase);
    
    io_err = 0;
    ios->IOSer.io_Unit = (APTR)_devbase->unit;
    ios->IOSer.io_Unit->unit_flags = UNITF_ACTIVE;
    ios->IOSer.io_Unit->unit_OpenCnt = 1;
    
    spawn_process(_devbase);
  }
  
  ios->IOSer.io_Error = io_err;
  return io_err;
}

int __UserDevClose(struct IOExtSer *iotd)
{
  KPrintF("UserDevClose\n");
  return 0;
}

int __UserDevCleanup(void)
{
  return 0;
}

ADDTABL_1(__BeginIO,a1);
__attribute__((saveds)) void __BeginIO(struct IOStdReq *request)
{
  ServiceRequest(request, _devbase);
  return;
}

ADDTABL_1(__AbortIO,a1);
__attribute__((saveds)) void __AbortIO(struct IOStdReq* request)
{
  struct DevUnit *unit;

  KPrintF("AbortIO\n");
  
  if (!request) return;

  while (_devbase->lock) {
    KPrintF("abort locked\n");
  }

  _devbase->lock = 1;
  if (_devbase->pending_req == (struct IOExtSer*)request) {
    //_devbase->pending_req = NULL;
  }
  _devbase->lock = 0;
  
  request->io_Error = IOERR_NOCMD;
  
  if (!(request->io_Flags & IOF_QUICK)) ReplyMsg((APTR)request);
}

