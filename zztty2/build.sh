export CROSSPATH=$HOME/code/amiga/m68k-amigaos
export GCCLIBPATH=$CROSSPATH/lib/gcc-lib/m68k-amigaos/2.95.3
export PATH=$CROSSPATH/bin:$GCCLIBPATH:$PATH
export GCC_EXEC_PREFIX=m68k-amigaos
export LIBS=$CROSSPATH/lib

# http://fengestad.no/m68k-amigaos-toolchain/

m68k-amigaos-gcc -m68000 -O2 -ramiga-dev -noixemul -fbaserel -I$CROSSPATH/m68k-amigaos/sys-include -I$CROSSPATH/os-include -L$LIBS -L$LIBS/gcc-lib/m68k-amigaos/2.95.3/ -L$CROSSPATH -c device.c

m68k-amigaos-gcc -m68000 -O2 -ramiga-dev -noixemul -fbaserel -I$CROSSPATH/m68k-amigaos/sys-include -I$CROSSPATH/os-include -L$LIBS -L$LIBS/gcc-lib/m68k-amigaos/2.95.3/ -L$CROSSPATH -c unit.c

m68k-amigaos-gcc -m68000 -O2 -ramiga-dev -noixemul -fbaserel -I$CROSSPATH/m68k-amigaos/sys-include -I$CROSSPATH/os-include -L$LIBS -L$LIBS/gcc-lib/m68k-amigaos/2.95.3/ -L$CROSSPATH -c request.c

m68k-amigaos-gcc -m68000 -msmall-code -O1 -Wno-uninitialized -o zztty.device -ramiga-dev -noixemul -fbaserel -I$CROSSPATH/m68k-amigaos/sys-include -I$CROSSPATH/os-include -L$LIBS -L$LIBS/gcc-lib/m68k-amigaos/2.95.3/ -L$CROSSPATH device.c request.c unit.c -ldebug
