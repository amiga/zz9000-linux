/*

File: request.c
Author: Neil Cafferkey
Copyright (C) 2001-2003 Neil Cafferkey

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.

*/


#include <exec/types.h>
#include <exec/errors.h>

#include "initializers.h"
#include "newstyle.h"

#include <proto/exec.h>
#include <proto/utility.h>

#include "device.h"
#include "uart.h"

#include "request_protos.h"
#include "unit_protos.h"


static VOID CmdInvalid(struct IORequest *request,struct DevBase *base);
static VOID CmdRead(struct IOStdReq *request,struct DevBase *base);
static VOID CmdWrite(struct IOStdReq *request,struct DevBase *base);
static VOID CmdReset(struct IOExtSer *request,struct DevBase *base);
static VOID CmdClear(struct IOStdReq *request,struct DevBase *base);
static VOID CmdQuery(struct IOExtSer *request,struct DevBase *base);
static ULONG StrLen(const TEXT *s);


static const UWORD supported_commands[]=
{
   CMD_RESET,
   CMD_READ,
   CMD_WRITE,
   CMD_CLEAR,
   CMD_FLUSH,
   SDCMD_QUERY,
   SDCMD_SETPARAMS,
   0
};

VOID ServiceRequest(struct IOStdReq *request, struct DevBase *base)
{
  switch(request->io_Command)
    {
    case CMD_RESET:
      KPrintF("CMD_RESET\n");
      CmdReset((APTR)request,base);
      break;
    case CMD_READ:
      CmdRead(request,base);
      break;
    case CMD_WRITE:
      //KPrintF("CMD_WRITE\n");
      CmdWrite(request,base);
      break;
    case CMD_CLEAR:
      KPrintF("CMD_CLEAR\n");
      CmdClear(request,base);
      break;
    case CMD_FLUSH:
      KPrintF("CMD_FLUSH\n");
      CmdFlush(request,base);
      break;
    case SDCMD_QUERY:
      // TODO figure out what it does here
      //KPrintF("CMD_QUERY\n");
      CmdQuery((APTR)request,base);
      break;
    case SDCMD_SETPARAMS:
      KPrintF("CMD_SETPARAMS\n");
      CmdSetParams((APTR)request,base);
      break;
    default:
      KPrintF("CmdInvalid\n");
      CmdInvalid((APTR)request,base);
    }
}

static VOID CmdInvalid(struct IORequest *request,struct DevBase *base)
{
   request->io_Error=IOERR_NOCMD;

   if((request->io_Flags&IOF_QUICK)==0)
      ReplyMsg((APTR)request);
}

static VOID CmdReset(struct IOExtSer *request,struct DevBase *base)
{
   struct DevUnit *unit;

   unit=(APTR)request->IOSer.io_Unit;
   if((unit->flags&UNITF_SHARED)!=0)
      request->io_SerFlags=SERF_SHARED;
   else
      request->io_SerFlags=0;

   if((request->IOSer.io_Flags&IOF_QUICK)==0)
      ReplyMsg((APTR)request);
}

static VOID CmdRead(struct IOStdReq *request, struct DevBase *base)
{
  struct DevUnit *unit;

  request->io_Actual = request->io_Length; // FIXME
  request->io_Error = 0;
  
  /*KPrintF("BASE %lx\n", base);
  KPrintF("REQADDR %lx\n", request);
  KPrintF("REQDATA %lx\n", request->io_Data);
  KPrintF("REQLEN %lx\n", request->io_Length);*/

  //Disable();
  
  if (request->io_Length!=0 && request->io_Data!=0) {

    // FIXME
    request->io_Message.mn_Node.ln_Type = NT_MESSAGE;
    request->io_Flags &= ~IOF_QUICK;

    KPrintF("RQ\n");
    
    while (base->lock) {
      KPrintF("cmd locked\n");
    }
  
    base->lock = 1;
    if (base->pending_req && request!=(struct IOStdReq*)base->pending_req) {
      // abort pending request, replace with new one
      KPrintF("REPL\n");
      /*base->pending_req->IOSer.io_Data = 0;
      base->pending_req->IOSer.io_Actual = 0;
      base->pending_req->IOSer.io_Error = IOERR_ABORTED;
      base->pending_req = NULL;
      if (!(request->io_Flags & IOF_QUICK)) {
        ReplyMsg((APTR)base->pending_req);
        }*/
    }

    base->pending_req = (struct IOExtSer*)request;
    
    if (request->io_Flags & IOF_QUICK) {
      //KPrintF("QUIK\n");
      // this will take base->pending_req and set it to NULL again
      ReceiveData(base);
    } else {
      //KPrintF("NQUIK\n");
    }
    
    base->lock = 0;
  } else {
    KPrintF("REQ0\n");
    if (!(request->io_Flags & IOF_QUICK)) {
      ReplyMsg((APTR)request);
    }
  }
  
  //Enable();
}

static VOID CmdWrite(struct IOStdReq *request,struct DevBase *base)
{
  struct DevUnit *unit;

  unit=(APTR)request->io_Unit;

  /* Get length if null-terminated */

  // TODO dubious
  if (request->io_Length==-1)
    request->io_Length=StrLen(request->io_Data);
  request->io_Actual=request->io_Length;

  TransmitData((struct IOExtSer*)request);
}

static VOID CmdClear(struct IOStdReq *request,struct DevBase *base)
{
  if((request->io_Flags&IOF_QUICK)==0)
    ReplyMsg((APTR)request);
}

VOID CmdFlush(struct IOStdReq *request,struct DevBase *base)
{
  //FlushUnit((APTR)request->io_Unit,base);

  if((request->io_Flags&IOF_QUICK)==0)
    ReplyMsg((APTR)request);
}

static VOID CmdQuery(struct IOExtSer *request,struct DevBase *base)
{
   struct DevUnit *unit;

   unit=(APTR)request->IOSer.io_Unit;

   request->io_Status = 0;
   request->IOSer.io_Actual = 0;

   if ((request->IOSer.io_Flags&IOF_QUICK)==0)
      ReplyMsg((APTR)request);
}

VOID CmdSetParams(struct IOExtSer *request, struct DevBase *base)
{
   struct DevUnit *unit;
   BYTE error=0;
   UBYTE lcr,ser_flags,stop_bit_count,i,*term_array,ch;
   ULONG baud,ext_flags,*p,*term_map;

   unit=(APTR)request->IOSer.io_Unit;

   if((request->io_SerFlags&SERF_SHARED)!=0)
     unit->flags|=UNITF_SHARED;
   else
     unit->flags&=~UNITF_SHARED;
   
   baud=request->io_Baud;

   request->IOSer.io_Error=error;
   if((request->IOSer.io_Flags&IOF_QUICK)==0)
      ReplyMsg((APTR)request);
}

// TODO WTF
static ULONG StrLen(const TEXT *s)
{
   const TEXT *p;

   for(p=s;*p!='\0';p++);
   return p-s;
}

