/*

File: unit.c
Author: Neil Cafferkey
Copyright (C) 2001-2003 Neil Cafferkey

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.

*/

#include <exec/memory.h>
#include <exec/execbase.h>
#include <exec/errors.h>
#include <exec/interrupts.h>
#include <exec/tasks.h>
#include <dos/dostags.h>
#include <utility/tagitem.h>

#include <proto/exec.h>
#include <proto/alib.h>
#include <proto/utility.h>
#include <proto/dos.h>

#include <devices/serial.h>
#include <exec/io.h>

#include "device.h"

#include "unit_protos.h"
#include "request_protos.h"

void frame_proc();

// ZZ9000 Interrupt Handler (INT6)
__attribute__((saveds)) void dev_isr(register struct DevBase* db asm("a1")) {
  // signal main process that a packet is available
  if (db->task) {
    Signal((struct Task*)db->task, SIGBREAKF_CTRL_F);
  }
}

struct ProcInit
{
   struct Message msg;
   struct DevBase *db;
   BOOL  error;
   UBYTE pad[2];
};

struct ProcInit init;

VOID spawn_process(struct DevBase* db) {
  //struct Library* DOSBase;
  struct MsgPort* port;
  struct Library* DOSBase;
  
  KPrintF("spawn_process()\n");
  
  // crashes here
	if( (DOSBase = OpenLibrary("dos.library", 36)) ) {
    KPrintF("dos.library opened.\n");
  } else {
    KPrintF("dos.library failed to open.\n");
  }
  
  Delay(100);
  
  if (port = CreateMsgPort()) {
    KPrintF("CreateMsgPort OK\n");
    if (db->task = CreateNewProcTags(NP_Entry, frame_proc, NP_Name,
                                        "zztty", TAG_DONE)) {
      KPrintF("CreateNewProcTags OK\n");
      KPrintF("SPWNBASE: %lx\n", db);
      KPrintF("SPWNINIT: %lx\n", &init);
      
      init.error = 1;
      init.db = db;
      init.msg.mn_Length = sizeof(init);
      init.msg.mn_ReplyPort = port;

      //Delay(50);
      //KPrintF("zztty: handover db: %lx\n",init.db);

      PutMsg(&db->task->pr_MsgPort, (struct Message*)&init);
      WaitPort(port);
      
      KPrintF("WaitPort OK\n");

      if (!init.error) {
        // Register Interrupt (INT6) server
        if (db->db_int6 = AllocMem(sizeof(struct Interrupt), MEMF_PUBLIC|MEMF_CLEAR)) {
          USHORT hw_config = *(USHORT*)(0x50000004);
          
          db->db_int6->is_Node.ln_Type = NT_INTERRUPT;
          db->db_int6->is_Node.ln_Pri = -60;
          db->db_int6->is_Node.ln_Name = "zztty";
          db->db_int6->is_Data = (APTR)db;
          db->db_int6->is_Code = dev_isr;

          // 13 = INTB_EXTER
          Disable();
          AddIntServer(13, db->db_int6);
          Enable();
        
          KPrintF("zztty: INT6 server registered\n");

          // FIXME address
          // enable HW interrupt
          hw_config |= 1;
          *(volatile USHORT*)(0x50000004) = hw_config;
              
          KPrintF("zztty: ZZ interrupt enabled\n");
        } else {
          KPrintF("zztty: failed to alloc INT6 struct\n");
          // FIXME end process
        }
      } else {
        KPrintF("zztty:process startup error\n");
      }
    } else {
      KPrintF("zztty:couldn't create process\n");
    }
    DeleteMsgPort(port);
  }

}

struct DevUnit *CreateUnit(ULONG unit_num, struct DevBase *db)
{
  struct DevUnit *unit;
  BOOL success=TRUE;
  
  SysBase = *(struct ExecBase **)4L;

  KPrintF("CreateUnit\n");
  
  unit = AllocMem(sizeof(struct DevUnit),MEMF_CLEAR);

  if (unit==NULL) success=FALSE;

  KPrintF("CreateUnit OK\n");
   
  if (success) {
    unit->unit_num = unit_num;
    unit->device = db;
  }
  
  return unit;
}

VOID DeleteUnit(struct DevUnit* unit, struct DevBase* base)
{
  UBYTE i;
  struct CardHandle *card_handle;

  KPrintF("DeleteUnit\n");
  return;
}

VOID ResetUnit(struct IOExtSer* request, struct DevBase* base)
{
  struct DevUnit *unit;
  UBYTE flags,i;
  struct IOStdReq *old_request;

  return;
   
  unit=(APTR)request->IOSer.io_Unit;

  /* Preserve quick bit */

  flags=request->IOSer.io_Flags;
  request->IOSer.io_Flags|=IOF_QUICK;

  request->IOSer.io_Flags=flags;
}

VOID FlushUnit(struct DevUnit *unit,struct DevBase *base)
{
   KPrintF("FlushUnit\n");
}

__attribute__((saveds)) VOID TransmitData(struct IOExtSer* request)
{
   UBYTE data,i;
   ULONG len = request->IOSer.io_Length;
   UBYTE* buffer = request->IOSer.io_Data;
   volatile UWORD* write_reg = (volatile UWORD*)(0x500000e0); // FIXME

   for (i=0; i<len; i++) {
     data=*(buffer++);
     *write_reg = (UWORD)data;
   }
   
   request->IOSer.io_Actual = len;
   request->IOSer.io_Flags=0;
   ReplyMsg((APTR)request);
}

__attribute__((saveds)) VOID ReceiveData(struct DevBase* db)
{
  UBYTE data;
  volatile ULONG *read_reg;
  ULONG rawin = 0;
  ULONG timeout = 0;
  struct IOExtSer* request;

  // FIXME
  read_reg = (volatile ULONG*)(0x500000e0);

  request = (struct IOExtSer*)db->pending_req;
  db->pending_req = NULL;

  //KPrintF("RXREQADDR %lx\n", request);
  //KPrintF("RXREQDATA %lx\n", request->IOSer.io_Data);
  //KPrintF("REQLEN %lx\n", request->IOSer.io_Length);
  
  if (request && request->IOSer.io_Data) {
    UBYTE* buffer = request->IOSer.io_Data;
    ULONG len = request->IOSer.io_Length;
    
    // TODO implement a bit of slack and try to get more
    
    if ((rawin = *read_reg)) {
      data = rawin>>24;
      *(buffer)=data;

      //KPrintF("RX\n");
      
      request->IOSer.io_Actual = 1;
      request->IOSer.io_Error = 0;
      
    } else {
      request->IOSer.io_Actual = 0;
      *(buffer) = 0;
      //KPrintF("NRX1\n");
    }
  } else {
    KPrintF("NRX2\n");
  }
}

__attribute__((saveds)) void frame_proc() {
  ULONG wmask;  
  struct Process* proc;
  struct ProcInit* init;
  struct DevBase* db;
  ULONG recv = 0;
  struct ExecBase* SysBase = *(struct ExecBase **)4L;
  struct IOExtSer* request;

  KPrintF("zztty: frame_proc()\n");

  proc = (struct Process*)FindTask(NULL);
  WaitPort(&proc->pr_MsgPort);
  init = (struct ProcInit*)GetMsg(&proc->pr_MsgPort);
  
  init->error = 0;
  ReplyMsg((struct Message*)init);

  db = (struct DevBase*) init->db;
  wmask = SIGBREAKF_CTRL_F | SIGBREAKF_CTRL_C;
  
  while (1) {
    recv = Wait(wmask);
    
    while (db->lock) {
      KPrintF("RXLOCK\n");
    }
    db->lock = 1;
    request = (struct IOExtSer*)db->pending_req;
    
    ReceiveData(db);
    if (request && !(request->IOSer.io_Flags & IOF_QUICK)) {
      ReplyMsg((APTR)request);
    }
    db->lock = 0;
  }
}
