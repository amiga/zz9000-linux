/*
 * MNT ZZ9000 Amiga Graphics and ARM Coprocessor SDK
 *            "zz9k" Amiga CLI tool for loading and executing
 *            ARM applications
 *
 * Copyright (C) 2019, Lukas F. Hartmann <lukas@mntre.com>
 *                     MNT Research GmbH, Berlin
 *                     https://mntre.com
 *
 * More Info: https://mntre.com/zz9000
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * GNU General Public License v3.0 or later
 *
 * https://spdx.org/licenses/GPL-3.0-or-later.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/expansion.h>
#include <proto/graphics.h>
#include <libraries/expansion.h>
#include <libraries/configvars.h>
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <intuition/intuitionbase.h>
#include <intuition/screens.h>
#include <cybergraphx/cybergraphics.h>
#include <proto/cybergraphics.h>
#include <devices/audio.h>
#include <devices/input.h>
#include <exec/ports.h>
#include <exec/interrupts.h>
#include <exec/memory.h>

#include "zz9000.h"

#define uint8_t unsigned char
#define int16_t signed short
#define uint16_t unsigned short
#define uint32_t unsigned long
#define int32_t signed long

volatile MNTZZ9KRegs* regs;

struct Screen* zz9k_screen = NULL;
struct Screen* active_screen = NULL;

static int exit_requested = 0;

void open_screen(int w, int h) {
  uint32_t bmid=BestCModeIDTags(CYBRBIDTG_NominalWidth, w,
                                CYBRBIDTG_NominalHeight, h,
                                CYBRBIDTG_Depth, 24,
                                TAG_DONE);

  printf("Mode ID: %lx\n", bmid);

  zz9k_screen = OpenScreenTags(NULL,
                               SA_Title,"ZZ9000 Linux Screen",
                               SA_DisplayID, bmid,
                               SA_Depth, 24,
                               TAG_DONE);

  printf("Planes[0]: %p\n", zz9k_screen->BitMap.Planes[0]);
  printf("BytesPerRow: %d\n", zz9k_screen->BitMap.BytesPerRow);
  printf("Rows: %d\n", zz9k_screen->BitMap.Rows);
  printf("Depth: %d\n", zz9k_screen->BitMap.Depth);
}

__saveds struct InputEvent *input_handler(__reg("a0") struct InputEvent *ielist, __reg("a1") struct MsgPort *port) {
  struct InputEvent *ie, *prev;
  volatile uint8_t* r = regs;

  ie = ielist;
  prev = NULL;

  while (ie) {
    if (ie->ie_Class == IECLASS_RAWKEY) {
      int code = ie->ie_Code;
      //printf("RAWKEY: %x\n", code);
      if (active_screen == zz9k_screen) {
        *((volatile uint16_t*)(r+0xe8)) = code;

        // FIXME HELP key exits
        if (code == 95) {
          exit_requested = 1;
        }
      }
    }

    ie = ie->ie_NextEvent;
  }

  return (ielist);
}

BOOL input_dev_open = FALSE;
struct MsgPort  *input_port = NULL;
struct Interrupt *input_irq_handler = NULL;
struct IOStdReq *input_req = NULL;
UBYTE NameString[]="zz9k input";

int setup_input() {
  int rc = 0;

  if (input_port = CreateMsgPort())
    if (input_irq_handler = AllocVec(sizeof(struct Interrupt),MEMF_CLEAR))
      if (input_req = (struct IOStdReq *) CreateIORequest(input_port,sizeof(struct IOStdReq)))
        if (!OpenDevice("input.device",0,(struct IORequest *)input_req,0))
          input_dev_open = TRUE;

  if (!input_dev_open) {
    printf("Error: Could not open input.device.\n");
  } else {
    input_irq_handler->is_Code = (APTR)input_handler;
    input_irq_handler->is_Data = (APTR)input_port;
    input_irq_handler->is_Node.ln_Name = "zz9k";
    input_irq_handler->is_Node.ln_Pri  = 60;

    input_req->io_Data = (APTR)input_irq_handler;
    input_req->io_Command = IND_ADDHANDLER;
    DoIO((struct IORequest *)input_req);

    printf("Input handler set up.\n");

    rc = 1;
  }

  return rc;
}

void cleanup() {
  if (zz9k_screen) {
    CloseScreen(zz9k_screen);
  }

  if (input_dev_open) {
    input_req->io_Data = (APTR)input_irq_handler;
    input_req->io_Command = IND_REMHANDLER;
    DoIO((struct IORequest *)input_req);

    CloseDevice((struct IORequest *)input_req);
    DeleteIORequest((struct IORequest *)input_req);
    FreeVec(input_irq_handler);
    DeleteMsgPort(input_port);
  }
}

void fix_vsync(MNTZZ9KRegs* registers) {
  // video control op: vsync
  *(uint16_t*)((uint32_t)registers+0x1000) = 0;
  *(uint16_t*)((uint32_t)registers+0x1002) = 1;
  *(uint16_t*)((uint32_t)registers+0x1004) = 5; // OP_VSYNC
  *(uint16_t*)((uint32_t)registers+0x1004) = 0;
  *(uint16_t*)((uint32_t)registers+0x1002) = 0;
}

int main(int argc, char** argv) {
  struct ConfigDev* cd = NULL;
  int screen_mode = 0;
  int keyboard_mode = 0;
  int verbose_mode = 0;
  int screen_w = 1024;
  int screen_h = 768;
  uint16_t oldx = 0;
  uint16_t oldy = 0;
  volatile uint8_t* r;
  uint16_t mousebtn = 0;
  uint16_t mousebtnold = 0;
  struct IntuitionBase *IntuitionBase;

  IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library", 33L);
  if (!IntuitionBase) {
    printf("Error: cannot open IntuitionBase v33.\n");
    exit(1);
  }

  // find a ZZ9000
  cd = (struct ConfigDev*)FindConfigDev(cd,0x6d6e,0x4);
  if (!cd) {
    cd = (struct ConfigDev*)FindConfigDev(cd,0x6d6e,0x3);
  }
  if (!cd) {
    printf("Error: MNT ZZ9000 not found.\n");
    exit(1);
  }
  printf("MNT ZZ9000 found.\n");

  //memory = (uint8_t*)(cd->cd_BoardAddr)+0x10000;
  regs = (volatile MNTZZ9KRegs*)(cd->cd_BoardAddr);
  r = regs;

  open_screen(screen_w, screen_h);

  if (!zz9k_screen) {
    printf("Error creating screen.\n");
    //exit(2);
  }

  // capture keypresses
  setup_input();

  // get screen bitmap address
  //uint32_t fb = ((uint32_t)zz9k_screen->BitMap.Planes[0])-(uint32_t)memory+(uint32_t)ZZ9K_MEM_START;
  //fb+=zz9k_screen->BarHeight*zz9k_screen->BitMap.BytesPerRow; // skip title bar

  // main loop
  while (1) {
    uint16_t mousex = *((volatile uint8_t*)0xdff00b);
    uint16_t mousey = *((volatile uint8_t*)0xdff00a);
    uint16_t mousexy = 0;

    volatile uint8_t* mreg = (volatile uint8_t*)0xbfe001;
    // LMB FIXME
    mousebtn = (!(*mreg&(1<<6)));
    // RMB FIXME
    mreg = (volatile uint8_t*)0xdff016;
    mousebtn |= ((!(*mreg&(1<<2)))<<1);

    mousexy = (((mousey-oldy)&0xff)<<8)|((mousex-oldx)&0xff);

    oldx = mousex;
    oldy = mousey;

    // TODO: rmb
    if (active_screen == zz9k_screen) {
      if (mousebtn!=mousebtnold) {
        *((volatile uint16_t*)(r+0xe6)) = mousebtn|(mousebtn<<8);
      }
    }

    mousebtnold = mousebtn;

    if (active_screen == zz9k_screen) {
      if (mousexy!=0) {
        // report mouse
        *((volatile uint16_t*)(r+0xe4)) = mousexy;
      }
    }

    if (exit_requested) break;

    if (active_screen != ((struct IntuitionBase *)IntuitionBase)->FirstScreen) {
      active_screen = ((struct IntuitionBase *)IntuitionBase)->FirstScreen;
      if (active_screen == zz9k_screen) {
        // FIXME
        // pan to the virtual screen instead (32MB offset)
        *((volatile uint16_t*)(r+0x0a)) = 0x0200;
        *((volatile uint16_t*)(r+0x0c)) = 0x0000;
        for (volatile int i=0; i<100; i++) {
          fix_vsync(r);
        }
      }
    }
  }

  cleanup();
}
