#!/bin/bash

set -e

mmdebstrap --architectures=armhf --components=main --variant="minbase" sid rootfs http://ftp.de.debian.org/debian

sudo chroot rootfs /bin/bash <<EOF
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true
export LC_ALL=C
export LANGUAGE=C
export LANG=C

echo "root:root" | chpasswd
passwd -d root # remove root password

# mount /proc if it doesn't exist
if [[ ! -e /proc/version ]]; then
  mount -t proc none /proc
fi

ln --force --symbolic /usr/share/zoneinfo/Europe/Berlin /etc/localtime
echo "locales locales/default_environment_locale select en_US.UTF-8" | debconf-set-selections
echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections
dpkg --configure -a

apt update
apt install -y apt apt-utils gnupg ca-certificates

# FIXME: where does exim4 come from??

apt install -y cpio bsdmainutils init-system-helpers procps gpgv debian-archive-keyring readline-common cron netbase iproute2 ifupdown isc-dhcp-client iptables iputils-ping locales less net-tools curl wget nano vim coreutils parted file git sudo console-setup console-data unicode-data kbd systemd libpam-systemd systemd-sysv bash-completion ncurses-term usbutils fbset netcat-traditional nfacct traceroute htop ncdu ntpdate ntp screen telnet dosfstools e2fsprogs dialog i2c-tools xserver-xorg-video-fbdev xserver-xorg-input-libinput xserver-xorg-input-evdev

# disable built-in sleep targets
systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

EOF
