#!/bin/bash

git clone --depth 1 https://github.com/u-boot/u-boot.git
cd u-boot

export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-

patch -p1 <../patches-uboot/mnt-zz9000-reset.patch
patch -p1 <../patches-uboot/mnt-zz9000-dts-makefile.patch
cp ../zynq-zz9000_defconfig ./configs/
cp ../zynq-zz9000.dts ./arch/arm/dts/

make zynq-zz9000_defconfig
cp ../uboot-config ./.config

# build u-boot.elf

make -j$(nproc)

