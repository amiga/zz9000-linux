/*
 * MNT ZZ9000 Network Driver (ZZ9000Net.device)
 * Copyright (C) 2016-2019, Lukas F. Hartmann <lukas@mntre.com>
 *                          MNT Research GmbH, Berlin
 *                          https://mntre.com
 *
 * Based on code copyright (C) 2018 Henryk Richter <henryk.richter@gmx.net>
 * Released under GPLv3+ with permission.
 *
 * More Info: https://mntre.com/zz9000
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * GNU General Public License v3.0 or later
 *
 * https://spdx.org/licenses/GPL-3.0-or-later.html
 */

#define DEVICE_MAIN

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/expansion.h>
#include <clib/exec_protos.h>
#include <clib/alib_protos.h>
#include <dos/dostags.h>
#include <utility/tagitem.h>
#include <exec/lists.h>
#include <exec/errors.h>
#include <exec/interrupts.h>
#include <exec/tasks.h>
#include <string.h>

#ifdef HAVE_VERSION_H
#include "version.h"
#endif

#include "device.h"
#include "macros.h"

static ULONG ZZ9K_REGS = 0;

__saveds void frame_proc();
char *frame_proc_name = "zzttyproc";

// ZZ9000 Interrupt Handler (INT6)
__saveds void dev_isr(__reg("a1") struct devbase* db) {
  // signal main process that a packet is available
  if (db->db_Proc) {
    Signal((struct Task*)db->db_Proc, SIGBREAKF_CTRL_F);
  }
}

struct ProcInit
{
   struct Message msg;
   struct devbase *db;
   BOOL  error;
   UBYTE pad[2];
};

__saveds struct Device *DevInit( ASMR(d0) DEVBASEP                  ASMREG(d0), 
                                   ASMR(a0) BPTR seglist              ASMREG(a0), 
				   ASMR(a6) struct Library *_SysBase  ASMREG(a6) )
{
	UBYTE*p;
	ULONG i;
	LONG  ok;

	p = ((UBYTE*)db) + sizeof(struct Library);
	i = sizeof(DEVBASETYPE)-sizeof(struct Library);
	while( i-- )
		*p++ = 0;

	db->db_SysBase = _SysBase;
	db->db_SegList = seglist;
	db->db_Flags   = 0;

	ok = 0;
	if( (DOSBase = OpenLibrary("dos.library", 36)) ) {
		if( (UtilityBase = OpenLibrary("utility.library", 37)) ) {
			ok = 0;

      struct ConfigDev* cd = NULL;
      USHORT fwrev = 0;

      if ((ExpansionBase = OpenLibrary("expansion.library", 0)) ) {
        // Find Z2 or Z3 model of MNT ZZ9000
        if ((cd = (struct ConfigDev*)FindConfigDev(cd,0x6d6e,0x4)) || (cd = (struct ConfigDev*)FindConfigDev(cd,0x6d6e,0x3))) {
          BPTR fh;
          
          D(("zztty: MNT ZZ9000 found.\n"));
          ZZ9K_REGS = (ULONG)cd->cd_BoardAddr;
          ok = 1;

        } else {
          D(("zztty: MNT ZZ9000 not found!\n"));
        }
				CloseLibrary(ExpansionBase);
      } else {
        D(("zztty: failed to open expansion.library!\n"));
      }
         
			if (!ok) {
				CloseLibrary(DOSBase);
				CloseLibrary(UtilityBase);
			}
		}
		else {
			D(("zztty: Could not open utility.library.\n"));
			CloseLibrary(DOSBase);
		}
	}
	else {
		D(("zztty: Could not open dos.library.\n"));
	}

	/* no hardware found, reject init */
	return (ok > 0) ? (struct Device*)db : (0);
}

__saveds LONG DevOpen( ASMR(a1) struct IORequest *ioreq           ASMREG(a1), 
                         ASMR(d0) ULONG unit                         ASMREG(d0), 
                         ASMR(d1) ULONG flags                        ASMREG(d1),
                         ASMR(a6) DEVBASEP                           ASMREG(a6) )
{
	LONG ok = 0,ret = IOERR_OPENFAIL;

	D(("zztty: DevOpen for %ld\n",unit));

	db->db_Lib.lib_OpenCnt++; /* avoid Expunge, see below for separate "unit" open count */
  
  if (unit==0 && db->db_Lib.lib_OpenCnt==1) {
    ioreq->io_Error = 0;
    ioreq->io_Unit = (struct Unit *)unit; // not a real pointer, but id integer
    ioreq->io_Device = (struct Device *)db;

    NewList(&db->db_ReadList);
    InitSemaphore(&db->db_ReadListSem);

    struct ProcInit init;
    struct MsgPort *port;

    if (port = CreateMsgPort()) {
      D(("zztty: Starting Process\n"));
      if (db->db_Proc = CreateNewProcTags(NP_Entry, frame_proc, NP_Name,
                                          frame_proc_name, TAG_DONE)) {
        init.error = 1;
        init.db = db;
        init.msg.mn_Length = sizeof(init);
        init.msg.mn_ReplyPort = port;

        Delay(50);
          
        D(("zztty: handover db: %lx\n",init.db));
          
        PutMsg(&db->db_Proc->pr_MsgPort, (struct Message*)&init);
        WaitPort(port);

        if (!init.error) {
          ok = 1;
          
          // Register Interrupt (INT6) server
          if (db->db_int6 = AllocMem(sizeof(struct Interrupt), MEMF_PUBLIC|MEMF_CLEAR)) {
            db->db_int6->is_Node.ln_Type = NT_INTERRUPT;
            db->db_int6->is_Node.ln_Pri = -60;
            db->db_int6->is_Node.ln_Name = "zztty";
            db->db_int6->is_Data = (APTR)db;
            db->db_int6->is_Code = dev_isr;

            // 13 = INTB_EXTER
            Disable();
            AddIntServer(13, db->db_int6);
            Enable();
        
            D(("zztty: INT6 server registered\n"));
            ret = 0;
            ok = 1;
              
            // enable HW interrupt
            //USHORT hw_config = *(USHORT*)(ZZ9K_REGS+0x04);
            //hw_config |= 1;
            //*(volatile USHORT*)(ZZ9K_REGS+0x04) = hw_config;
              
            //D(("zztty: ZZ interrupt enabled\n"));
          } else {
            D(("zztty: failed to alloc INT6 struct\n"));
            ret = IOERR_OPENFAIL;
            ok = 0;
            // FIXME end process
          }
        } else {
          D(("zztty: process startup error\n"));
          ret = IOERR_OPENFAIL;
          ok = 0;
        }
      } else {
        D(("zztty: couldn't create process\n"));
        ret = IOERR_OPENFAIL;
        ok = 0;
      }
      DeleteMsgPort(port);
    }
  } else {
    ret = IOERR_OPENFAIL;
    ok = 0;
  }

	if (ok) {
		ret = 0;
    db->db_Lib.lib_Flags &= ~LIBF_DELEXP;
	}

	if (ret == IOERR_OPENFAIL) {
		ioreq->io_Unit   = (0);
		ioreq->io_Device = (0);
		ioreq->io_Error  = ret;
		db->db_Lib.lib_OpenCnt--;
	}
	ioreq->io_Message.mn_Node.ln_Type = NT_REPLYMSG;

	D(("zztty: DevOpen return code %ld\n",ret));

	return ret;
}

__saveds BPTR DevClose(   ASMR(a1) struct IORequest *ioreq        ASMREG(a1),
                            ASMR(a6) DEVBASEP                       ASMREG(a6) )
{
	/* ULONG unit; */
	BPTR  ret = (0);

	D(("zztty: DevClose open count %ld\n",db->db_Lib.lib_OpenCnt));

	if( !ioreq )
		return ret;

	db->db_Lib.lib_OpenCnt--;

  if (db->db_Lib.lib_OpenCnt == 0) {
    // disable HW interrupt
    USHORT hw_config = *(USHORT*)(ZZ9K_REGS+0x04);
    hw_config &= 0xfffe;
    *(volatile USHORT*)(ZZ9K_REGS+0x04) = hw_config;
              
    D(("zztty: ZZ interrupt disabled\n"));
              
    Forbid();
    if (db->db_int6) {
      D(("zztty: Remove IntServer...\n"));
      RemIntServer(13, db->db_int6);
      db->db_int6 = 0;
    }
    if (db->db_Proc) {
      D(("zztty: End Proc...\n"));
      Signal((struct Task*)db->db_Proc, SIGBREAKF_CTRL_C);
      db->db_Proc = 0;
    }
    Permit();
  }

	ioreq->io_Device = (0);
	ioreq->io_Unit   = (struct Unit *)(-1);

	if (db->db_Lib.lib_Flags & LIBF_DELEXP)
		ret = DevExpunge(db);

	return ret;
}

__saveds BPTR DevExpunge( ASMR(a6) DEVBASEP                        ASMREG(a6) )
{
	BPTR seglist = db->db_SegList;

	if( db->db_Lib.lib_OpenCnt )
	{
		db->db_Lib.lib_Flags |= LIBF_DELEXP;
		return (0);
	}
  
  D(("zztty: Remove Device Node...\n"));
  Remove((struct Node*)db);

	CloseLibrary(DOSBase);
	CloseLibrary(UtilityBase);
	FreeMem( ((BYTE*)db)-db->db_Lib.lib_NegSize,(ULONG)(db->db_Lib.lib_PosSize + db->db_Lib.lib_NegSize));

	return seglist;
}

ULONG read_tty(struct IOExtSer *req);
ULONG write_tty(struct IOExtSer *req);

__saveds VOID DevBeginIO( ASMR(a1) struct IOExtSer *ioreq       ASMREG(a1),
                            ASMR(a6) DEVBASEP                       ASMREG(a6) )
{
	ULONG unit = (ULONG)ioreq->IOSer.io_Unit;
  int mtu;
  
	ioreq->IOSer.io_Message.mn_Node.ln_Type = NT_MESSAGE;
  ioreq->IOSer.io_Error = 0;

	D(("BeginIO command %ld unit %ld\n",(LONG)ioreq->IOSer.io_Command,unit));

	/*switch( ioreq->IOSer.io_Command ) {
  case CMD_READ:
    // not quick, add request to reader list
    // will be handled on interrupts by frame_proc
    ioreq->IOSer.io_Flags &= ~IOF_QUICK;
    ObtainSemaphore(&db->db_ReadListSem);
    AddHead((struct List*)&db->db_ReadList, (struct Node*)ioreq);
    ReleaseSemaphore(&db->db_ReadListSem);
    ioreq = NULL;
    break;
  case CMD_WRITE: {
    ULONG res = write_tty(ioreq);
    ReplyMsg((APTR)ioreq);
    break;
  }
    
  default:
    {
      ioreq->IOSer.io_Error = IOERR_ABORTED; // FIXME
      break;
    }
  }*/

	if (ioreq) {
		DevTermIO(db, (struct IORequest*)ioreq);
  }
}

__saveds LONG DevAbortIO( ASMR(a1) struct IORequest *ioreq        ASMREG(a1),
                            ASMR(a6) DEVBASEP                       ASMREG(a6) )
{
	LONG   ret = 0;
	D(("zztty: AbortIO on %lx\n",(ULONG)ioreq));
  
  Remove((struct Node*)ioreq);
	ioreq->io_Error = IOERR_ABORTED;
	ReplyMsg((struct Message*)ioreq);
	return ret;
}

void DevTermIO( DEVBASEP, struct IORequest *ioreq )
{
  if (!(ioreq->io_Flags & IOF_QUICK)) {
    ReplyMsg((struct Message *)ioreq);
  } else {
    ioreq->io_Message.mn_Node.ln_Type = NT_REPLYMSG;
  }
}

ULONG read_tty(struct IOExtSer *request)
{
  UBYTE data;
  volatile ULONG *read_reg;
  ULONG rawin = 0;

  // FIXME
  read_reg = (volatile ULONG*)(0x500000e0);
  request->IOSer.io_Actual = 0;
  
  if (request && request->IOSer.io_Data) {
    UBYTE* buffer = request->IOSer.io_Data;
    ULONG len = request->IOSer.io_Length;
    
    // TODO implement a bit of slack and try to get more
    
    if ((rawin = *read_reg)) {
      data = rawin>>24;
      *(buffer)=data;

      //KPrintF("RX %d\n",(int)data);
      
      request->IOSer.io_Actual = 1;
      request->IOSer.io_Error = 0;
      //ReplyMsg((APTR)request);
    } else {
      D(("NRX1\n"));
    }
  } else {
    D(("NRX2\n"));
  }

  return 0;
}

ULONG write_tty(struct IOExtSer *request)
{
   UBYTE data,i;
   ULONG len = request->IOSer.io_Length;
   UBYTE* buffer = request->IOSer.io_Data;
   volatile UWORD* write_reg = (volatile UWORD*)(0x500000e0); // FIXME

   KPrintF("TX\n");
      
   for (i=0; i<len; i++) {
     data=*(buffer++);
     *write_reg = (UWORD)data;
   }
   
   request->IOSer.io_Actual = len;
   
   return 0;
}

__saveds void frame_proc() {
  ULONG wmask;
  ULONG recv = 0;
  
  struct Process* proc;
  struct ProcInit* init;
  
  D(("zztty: frame_proc()\n"));

  {
    struct { void *db_SysBase; } *db = (void*)0x4;

    proc = (struct Process*)FindTask(NULL);
    WaitPort(&proc->pr_MsgPort);
    init = (struct ProcInit*)GetMsg(&proc->pr_MsgPort);
  
    init->error = 0;
    ReplyMsg((struct Message*)init);
  }
  
  struct devbase* db = init->db;
  wmask = SIGBREAKF_CTRL_F | SIGBREAKF_CTRL_C;

  // wait for the first packet
  recv = Wait(wmask);
  
  while (1) {
    D(("zztty: irqloop\n"));
      
    struct IOExtSer *ior;
    BOOL receiver_found = 0;
    int processed = 0;
    
    // wait for signal from our interrupt handler
    // remove this to use polled-IO

    if (recv & SIGBREAKF_CTRL_C) {
      D(("zztty: process end\n"));
      break;
    }

    ObtainSemaphore(&db->db_ReadListSem);
    for (ior = (struct IOExtSer *)db->db_ReadList.lh_Head;
         ior->IOSer.io_Message.mn_Node.ln_Succ;
         ior = (struct IOExtSer *)ior->IOSer.io_Message.mn_Node.ln_Succ) {
      
      read_tty(ior);
      
      Remove((struct Node*)ior);
      ReplyMsg((struct Message *)ior);
      processed = 1;
    }
    ReleaseSemaphore(&db->db_ReadListSem);

    recv = Wait(wmask);
  }
}

