// SPDX-License-Identifier: GPL-2.0
/*
 * uio_zz9000.c -- kernel space driver for MNT ZZ9000 Zorro graphics/multifunction card
 *
 * Copyright (C) 2020 MNT Research GmbH / Lukas F. Hartmann <lukas@mntre.com>
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/time.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <asm/outercache.h>
#include <asm/cacheflush.h>
#include <linux/dma-mapping.h>
#include <linux/fb.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>

#include "zz9000_gfx.h"

enum zz_video_modes {
	ZZVMODE_1280x720,
	ZZVMODE_800x600,
	ZZVMODE_640x480,
	ZZVMODE_1024x768,
	ZZVMODE_1280x1024,
	ZZVMODE_1920x1080_60,
	ZZVMODE_720x576,		// 50Hz
	ZZVMODE_1920x1080_50,	// 50Hz
	ZZVMODE_720x480,
	ZZVMODE_640x512,
	ZZVMODE_NUM,
};

struct zz_video_mode {
	int hres, vres;
	int hstart, hend, hmax;
	int vstart, vend, vmax;
	int polarity;
	int mhz, phz, vhz;
	int hdmi;
};

struct zz_video_mode preset_video_modes[ZZVMODE_NUM] = {
	//	HRES		VRES	HSTART	HEND	HMAX	VSTART	VEND	VMAX	POLARITY	MHZ		PIXELCLOCK HZ	VERTICAL HZ		HDMI
	{	1280,		720,	1390,	1430,	1650,	725,	730,	750,	0,			75,		75000000,		60,				0 },
	{	800,		600,	840,	968,	1056,	601,	605,	628,	0,			40,		40000000,		60,				0 },
	{	640,		480,	656,	752,	800,	490,	492,	525,	0,			25,		25175000,		60,				0 },
	{	1024,		768,	1048,	1184,	1344,	771,	777,	806,	0,			65,		65000000,		60,				0 },
	{	1280,		1024,	1328,	1440,	1688,	1025,	1028,	1066,	0,			108,		108000000,		60,				0 },
	{	1920,		1080,	2008,	2052,	2200,	1084,	1089,	1125,	0,			150,	150000000,		60,				0 },
	{	720,		576,	732,	796,	864,	581,	586,	625,	1,			27,		27000000,		50,				0 },
	{	1920,		1080,	2448,	2492,	2640,	1084,	1089,	1125,	0,			150,	150000000,		50,				0 },
	{	720,		480,	720,	752,	800,	490,	492,	525,	0,			25,		25175000,		60,				0 },
	{	640,		512,	840,	968,	1056,	601,	605,	628,	0,			40,		40000000,		60,				0 },
};

// copied from amikbd.c
static unsigned char amiga_keymap[0x78] = {
	[0]	 = KEY_GRAVE,
	[1]	 = KEY_1,
	[2]	 = KEY_2,
	[3]	 = KEY_3,
	[4]	 = KEY_4,
	[5]	 = KEY_5,
	[6]	 = KEY_6,
	[7]	 = KEY_7,
	[8]	 = KEY_8,
	[9]	 = KEY_9,
	[10]	 = KEY_0,
	[11]	 = KEY_MINUS,
	[12]	 = KEY_EQUAL,
	[13]	 = KEY_BACKSLASH,
	[15]	 = KEY_KP0,
	[16]	 = KEY_Q,
	[17]	 = KEY_W,
	[18]	 = KEY_E,
	[19]	 = KEY_R,
	[20]	 = KEY_T,
	[21]	 = KEY_Y,
	[22]	 = KEY_U,
	[23]	 = KEY_I,
	[24]	 = KEY_O,
	[25]	 = KEY_P,
	[26]	 = KEY_LEFTBRACE,
	[27]	 = KEY_RIGHTBRACE,
	[29]	 = KEY_KP1,
	[30]	 = KEY_KP2,
	[31]	 = KEY_KP3,
	[32]	 = KEY_A,
	[33]	 = KEY_S,
	[34]	 = KEY_D,
	[35]	 = KEY_F,
	[36]	 = KEY_G,
	[37]	 = KEY_H,
	[38]	 = KEY_J,
	[39]	 = KEY_K,
	[40]	 = KEY_L,
	[41]	 = KEY_SEMICOLON,
	[42]	 = KEY_APOSTROPHE,
	[43]	 = KEY_BACKSLASH,
	[45]	 = KEY_KP4,
	[46]	 = KEY_KP5,
	[47]	 = KEY_KP6,
	[48]	 = KEY_102ND,
	[49]	 = KEY_Z,
	[50]	 = KEY_X,
	[51]	 = KEY_C,
	[52]	 = KEY_V,
	[53]	 = KEY_B,
	[54]	 = KEY_N,
	[55]	 = KEY_M,
	[56]	 = KEY_COMMA,
	[57]	 = KEY_DOT,
	[58]	 = KEY_SLASH,
	[60]	 = KEY_KPDOT,
	[61]	 = KEY_KP7,
	[62]	 = KEY_KP8,
	[63]	 = KEY_KP9,
	[64]	 = KEY_SPACE,
	[65]	 = KEY_BACKSPACE,
	[66]	 = KEY_TAB,
	[67]	 = KEY_KPENTER,
	[68]	 = KEY_ENTER,
	[69]	 = KEY_ESC,
	[70]	 = KEY_DELETE,
	[74]	 = KEY_KPMINUS,
	[76]	 = KEY_UP,
	[77]	 = KEY_DOWN,
	[78]	 = KEY_RIGHT,
	[79]	 = KEY_LEFT,
	[80]	 = KEY_F1,
	[81]	 = KEY_F2,
	[82]	 = KEY_F3,
	[83]	 = KEY_F4,
	[84]	 = KEY_F5,
	[85]	 = KEY_F6,
	[86]	 = KEY_F7,
	[87]	 = KEY_F8,
	[88]	 = KEY_F9,
	[89]	 = KEY_F10,
	[90]	 = KEY_KPLEFTPAREN,
	[91]	 = KEY_KPRIGHTPAREN,
	[92]	 = KEY_KPSLASH,
	[93]	 = KEY_KPASTERISK,
	[94]	 = KEY_KPPLUS,
	[95]	 = KEY_HELP,
	[96]	 = KEY_LEFTSHIFT,
	[97]	 = KEY_RIGHTSHIFT,
	[98]	 = KEY_CAPSLOCK,
	[99]	 = KEY_LEFTCTRL,
	[100]	 = KEY_LEFTALT,
	[101]	 = KEY_RIGHTALT,
	[102]	 = KEY_LEFTMETA,
	[103]	 = KEY_RIGHTMETA
};


static struct device *dev;
static struct task_struct *zorro_thread;
static dma_addr_t zz9000_dma_handle;

static void zz9000_release(struct device *dev)
{
	dev_info(dev, "zz9000_release()");
}

volatile uint32_t* mntz;
volatile uint32_t* vdma;
volatile uint32_t* clkw;

uint32_t* main_buffer_virt;
uint32_t* main_buffer;
uint32_t* main_vdma;

#define ZZTTY_MINORS 1
struct tty_struct* zztty_tty[10];
static int zztty_open_count = 0;

static struct input_dev *zzkbd_dev;
static struct input_dev *zzmouse_dev;

void zz9000_dump_status(void)
{
	printk("zz9000: MNTZ status: %x\n", mntz[3]);
}

void zz9000_allocate_buffer(void)
{
	// TODO: allocate CMA
	dma_set_coherent_mask(dev, DMA_BIT_MASK(32));
	// allocate 64MB block shared with amiga
	main_buffer_virt = dma_alloc_coherent(dev, 1024*1024*64, &zz9000_dma_handle, GFP_KERNEL);

	if (main_buffer_virt) {
		main_buffer = (uint32_t*)virt_to_phys(main_buffer_virt);
		main_vdma = main_buffer;
		printk("zz9000_allocate_buffer: success! %x (%x)\n",(uint32_t)main_buffer_virt,(uint32_t)main_buffer);
	} else {
		printk("zz9000_allocate_buffer: failure!\n");
	}
}

#define MNTZ_BASE 0x43000000
#define XPAR_VIDEO_AXI_VDMA_0_BASEADDR 0x83000000
#define XPAR_CLK_WIZ_0_BASEADDR 0x83C00000

#define MNT_REG_BASE					0x000000
#define MNT_FB_BASE						0x010000

#define MNTVA_COLOR_8BIT		 0
#define MNTVA_COLOR_16BIT565 1
#define MNTVA_COLOR_32BIT		 2
#define MNTVA_COLOR_1BIT		 3
#define MNTVA_COLOR_15BIT		 4

#define MNTVF_OP_UNUSED 12
#define MNTVF_OP_SPRITE_XY 13
#define MNTVF_OP_SPRITE_ADDR 14
#define MNTVF_OP_SPRITE_DATA 15
#define MNTVF_OP_MAX 6
#define MNTVF_OP_HS 7
#define MNTVF_OP_VS 8
#define MNTVF_OP_POLARITY 10
#define MNTVF_OP_SCALE 4
#define MNTVF_OP_DIMENSIONS 2
#define MNTVF_OP_COLORMODE 1

#define MNT_REG_VMODE			 MNT_REG_BASE+0x02
#define MNT_REG_CONFIG		 MNT_REG_BASE+0x04
#define MNT_REG_SPRITEX		 MNT_REG_BASE+0x06
#define MNT_REG_SPRITEY		 MNT_REG_BASE+0x08
#define MNT_REG_PAN_HI		 MNT_REG_BASE+0x0a
#define MNT_REG_PAN_LO		 MNT_REG_BASE+0x0c
#define MNT_REG_VCAP_VMODE MNT_REG_BASE+0x0e
#define MNT_REG_X1				 MNT_REG_BASE+0x10
#define MNT_REG_Y1				 MNT_REG_BASE+0x12
#define MNT_REG_X2				 MNT_REG_BASE+0x14
#define MNT_REG_Y2				 MNT_REG_BASE+0x16
#define MNT_REG_BLITTER_DST_PITCH	 MNT_REG_BASE+0x18
#define MNT_REG_X3				 MNT_REG_BASE+0x1a
#define MNT_REG_Y3				 MNT_REG_BASE+0x1c
#define MNT_REG_RGB1_HI		 MNT_REG_BASE+0x1e
#define MNT_REG_RGB1_LO		 MNT_REG_BASE+0x20
#define MNT_REG_BLITTER_SRC_HI		 MNT_REG_BASE+0x28
#define MNT_REG_BLITTER_SRC_LO		 MNT_REG_BASE+0x2a
#define MNT_REG_BLITTER_DST_HI		 MNT_REG_BASE+0x2c
#define MNT_REG_BLITTER_DST_LO		 MNT_REG_BASE+0x2e
#define MNT_REG_BLITTER_COLORMODE	 MNT_REG_BASE+0x30
#define MNT_REG_BLITTER_SRC_PITCH	 MNT_REG_BASE+0x32
#define MNT_REG_BLITTER_USER1			 MNT_REG_BASE+0x40
#define MNT_REG_BLITTER_USER2			 MNT_REG_BASE+0x42
#define MNT_REG_BLITTER_USER3			 MNT_REG_BASE+0x44
#define MNT_REG_BLITTER_USER4			 MNT_REG_BASE+0x46
#define MNT_REG_SPRITE_BITMAP			 MNT_REG_BASE+0x48
#define MNT_REG_RGB2_HI		 MNT_REG_BASE+0x34
#define MNT_REG_RGB2_LO		 MNT_REG_BASE+0x36

#define MNT_REG_OP_FILLRECT			 MNT_REG_BASE+0x22
#define MNT_REG_OP_COPYRECT			 MNT_REG_BASE+0x24
#define MNT_REG_OP_PATTERNRECT	 MNT_REG_BASE+0x26
#define MNT_REG_OP_P2CRECT			 MNT_REG_BASE+0x38
#define MNT_REG_OP_P2DRECT			 MNT_REG_BASE+0x3c
#define MNT_REG_OP_DRAWLINE			 MNT_REG_BASE+0x3a
#define MNT_REG_OP_INVERTRECT		 MNT_REG_BASE+0x3e

#define MNT_REG_SPRITE_COLORS		 MNT_REG_BASE+0x4a

#define MNT_REG_FW_VERSION		MNT_REG_BASE+0xc0
#define MNT_REG_USB_STATUS		MNT_REG_BASE+0xda
#define MNT_REG_USB_CAPACITY	MNT_REG_BASE+0xdc

#define MNT_REG_TTY_GET	MNT_REG_BASE+0xe0
#define MNT_REG_TTY_PUT	MNT_REG_BASE+0xe0
#define MNT_REG_MOUSE_XY	MNT_REG_BASE+0xe4
#define MNT_REG_MOUSE_BUTTONS	MNT_REG_BASE+0xe6
#define MNT_REG_KEYBOARD	MNT_REG_BASE+0xe8

void mntzorro_write(int reg, uint32_t data)
{
	//printk("zz9000: MNTZ[%d] <- %x\n", reg, data);
	mntz[reg] = data;
}

void zz9000_setup_clock(int mhz)
{
	uint32_t mul = 11;
	uint32_t div = 1;
	uint32_t otherdiv = 11;
	uint32_t phase, duty, divide, muldiv;

	printk("zz9000_setup_clock: %d MHz\n", mhz);

	// Multiply/divide 100mhz fabric clock to desired pixel clock
	if (mhz == 50) {
		mul = 15;
		div = 1;
		otherdiv = 30;
	} else if (mhz == 40) {
		mul = 14;
		div = 1;
		otherdiv = 35;
	} else if (mhz == 75) {
		mul = 15;
		div = 1;
		otherdiv = 20;
	} else if (mhz == 65) {
		mul = 13;
		div = 1;
		otherdiv = 20;
	} else if (mhz == 27) {
		mul = 27;
		div = 2;
		otherdiv = 50;
	}	 else if (mhz == 54) {
		mul = 27;
		div = 1;
		otherdiv = 50;
	} else if (mhz == 150) {
		mul = 15;
		div = 1;
		otherdiv = 10;
	} else if (mhz == 25) { // 25.205
		mul = 15;
		div = 1;
		otherdiv = 60;
	} else if (mhz == 108) {
		mul = 54;
		div = 5;
		otherdiv = 10;
	}

	clkw[0x200/4] = (mul << 8) | div;
	clkw[0x208/4] = otherdiv;

	// "load configuration"
	clkw[0x25c/4] = 3;

	printk("zz9000_setup_clock done: %d:%d:%d\n", mul,div,otherdiv);

	phase = clkw[0x20C/4];
	printk("CLK phase: %u\n", phase);
	duty = clkw[0x210/4];
	printk("CLK duty: %u\n", duty);
	divide = clkw[0x208/4];
	printk("CLK divide: %u\n", divide);
	muldiv = clkw[0x200/4];
	printk("CLK muldiv: %u\n", muldiv);
}


#define XAXIVDMA_TX_OFFSET			0x00000000	/**< TX channel registers base */
#define XAXIVDMA_RX_OFFSET			0x00000030	/**< RX channel registers base */

// one for each channel
#define XAXIVDMA_CR_OFFSET			0x00000000	 /**< Channel control */
#define XAXIVDMA_SR_OFFSET			0x00000004	 /**< Status */
#define XAXIVDMA_CDESC_OFFSET		0x00000008	 /**< Current descriptor pointer */
#define XAXIVDMA_TDESC_OFFSET	0x00000010	 /**< Tail descriptor pointer */
#define XAXIVDMA_HI_FRMBUF_OFFSET	0x00000014	 /**< 32 FrameBuf Sel*/
#define XAXIVDMA_FRMSTORE_OFFSET	0x00000018	 /**< Frame Store */
#define XAXIVDMA_BUFTHRES_OFFSET	0x0000001C	 /**< Line Buffer Thres */

#define XAXIVDMA_MM2S_ADDR_OFFSET 0x00000050 /**< MM2S channel Addr */
#define XAXIVDMA_S2MM_ADDR_OFFSET 0x000000A0 /**< S2MM channel Addr */
#define XAXIVDMA_VFLIP_OFFSET			0x000000EC /**< Enable Vertical Flip Register */

#define XAXIVDMA_CR_RD_PTR_SHIFT		8		/**< Shift for read pointer number */

#define XAXIVDMA_VSIZE_MASK				0x00001FFF /**< Vertical size */
#define XAXIVDMA_HSIZE_MASK				0x0000FFFF /**< Horizontal size */
#define XAXIVDMA_STRIDE_MASK			0x0000FFFF /**< Stride size */
#define XAXIVDMA_FRMDLY_MASK			0x0F000000 /**< Frame delay */

#define XAXIVDMA_FRMDLY_SHIFT			24		 /**< Shift for frame delay */
#define XAXIVDMA_VSIZE_OFFSET					0x00000000	/**< Vertical size */
#define XAXIVDMA_HSIZE_OFFSET					0x00000004	/**< Horizontal size */
#define XAXIVDMA_STRD_FRMDLY_OFFSET		0x00000008	/**< Horizontal size */
#define XAXIVDMA_START_ADDR_OFFSET		0x0000000C	/**< Start of address */
#define XAXIVDMA_START_ADDR_LEN				0x00000004	/**< Each entry is 4 bytes */
#define XAXIVDMA_START_ADDR_MSB_OFFSET 0x00000010	 /**< Start of address */

#define XAXIVDMA_REGINDEX_MASK		0x00000001 /**< Register Index */

#define XAXIVDMA_CR_RUNSTOP_MASK		0x00000001 /**< Start/stop DMA channel */
#define XAXIVDMA_CR_TAIL_EN_MASK		0x00000002 /**< Tail ptr enable or Park */
#define XAXIVDMA_CR_RESET_MASK			0x00000004 /**< Reset channel */
#define XAXIVDMA_CR_SYNC_EN_MASK		0x00000008 /**< Gen-lock enable */
#define XAXIVDMA_CR_FRMCNT_EN_MASK	0x00000010 /**< Frame count enable */
#define XAXIVDMA_CR_FSYNC_SRC_MASK	0x00000060 /**< Fsync Source Select */
#define XAXIVDMA_CR_GENLCK_SRC_MASK 0x00000080 /**< Genlock Source Select */
#define XAXIVDMA_CR_RD_PTR_MASK			0x00000F00 /**< Read pointer number */
#define XAXIVDMA_CR_GENLCK_RPT_MASK 0x00008000 /**< GenLock Repeat */
#define XAXIVDMA_VFLIP_EN_MASK			0x00000001 /**< Vertical flip enable */

#define XAXIVDMA_SR_HALTED_MASK				0x00000001	/**< DMA channel halted */
#define XAXIVDMA_SR_IDLE_MASK					0x00000002	/**< DMA channel idle */
#define XAXIVDMA_SR_ERR_INTERNAL_MASK 0x00000010	/**< Datamover internal err */
#define XAXIVDMA_SR_ERR_SLAVE_MASK		0x00000020	/**< Datamover slave err */
#define XAXIVDMA_SR_ERR_DECODE_MASK		0x00000040	/**< Datamover decode err */
#define XAXIVDMA_SR_ERR_FSZ_LESS_MASK 0x00000080	/**< FSize Less Mismatch err */
#define XAXIVDMA_SR_ERR_LSZ_LESS_MASK 0x00000100	/**< LSize Less Mismatch err */
#define XAXIVDMA_SR_ERR_SG_SLV_MASK		0x00000200	/**< SG slave err */
#define XAXIVDMA_SR_ERR_SG_DEC_MASK		0x00000400	/**< SG decode err */
#define XAXIVDMA_SR_ERR_FSZ_MORE_MASK 0x00000800	/**< FSize More Mismatch err */
#define XAXIVDMA_SR_ERR_ALL_MASK			0x00000FF0	/**< All errors */

uint32_t vdma_read(uint32_t offs1, uint32_t offs2) {
	uint32_t addr = ((uint32_t)vdma)+offs1+offs2;
	uint32_t val = *((uint32_t*)addr);

	printk("zz9000 vdma read: %x -> %x\n",addr,val);
	return val;
}

void vdma_write(uint32_t offs1, uint32_t offs2, uint32_t val) {
	uint32_t addr = ((uint32_t)vdma)+offs1+offs2;
	printk("zz9000 vdma write: %x <- %x\n",addr,val);
	*((uint32_t*)addr) = val;
}

uint32_t zz9000_vdma_hsize;
uint32_t zz9000_vdma_vsize;
uint32_t zz9000_vdma_hdiv;
uint32_t zz9000_vdma_vdiv;
uint32_t zz9000_pan_offset;

void zz9000_setup_vdma(int hsize, int vsize, int hdiv, int vdiv)
{
	int halted, runstop;
	u32 stride = hsize * 4;
	uint32_t cfg;

	// TODO: init vdma
	printk("zz9000_setup_vdma\n");

	zz9000_vdma_hsize = hsize;
	zz9000_vdma_vsize = vsize;
	zz9000_vdma_hdiv = hdiv;
	zz9000_vdma_vdiv = vdiv;

	cfg = vdma_read(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET);

	// stop
	cfg &= ~XAXIVDMA_CR_RUNSTOP_MASK;
	vdma_write(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET, cfg);

	// reset
	/*vdma_write(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET,
						 cfg | XAXIVDMA_CR_RESET_MASK);
	msleep(1);
	vdma_write(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET,
						 cfg & ~XAXIVDMA_CR_RESET_MASK);
						 msleep(1);*/

	vsize = vsize / vdiv;

	cfg = vdma_read(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET) &
		~(XAXIVDMA_CR_TAIL_EN_MASK | XAXIVDMA_CR_SYNC_EN_MASK |
			XAXIVDMA_CR_FRMCNT_EN_MASK | XAXIVDMA_CR_RD_PTR_MASK);

	// circular
	cfg |= XAXIVDMA_CR_TAIL_EN_MASK;

	// start
	//cfg |= XAXIVDMA_CR_RUNSTOP_MASK;
	vdma_write(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET, cfg);

	// framebuffer start address
	vdma_write(XAXIVDMA_MM2S_ADDR_OFFSET,
						 XAXIVDMA_START_ADDR_OFFSET,
						 (uint32_t)main_vdma+zz9000_pan_offset);

	vdma_write(XAXIVDMA_MM2S_ADDR_OFFSET,
						 XAXIVDMA_STRD_FRMDLY_OFFSET,
						 (stride / hdiv));

	vdma_write(XAXIVDMA_MM2S_ADDR_OFFSET,
						 XAXIVDMA_HSIZE_OFFSET, stride / hdiv);

	// start
	cfg |= XAXIVDMA_CR_RUNSTOP_MASK;
	vdma_write(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET, cfg);

	vdma_write(XAXIVDMA_MM2S_ADDR_OFFSET,
						 XAXIVDMA_VSIZE_OFFSET, vsize);

	halted = vdma_read(XAXIVDMA_TX_OFFSET, XAXIVDMA_SR_OFFSET) &
		XAXIVDMA_SR_HALTED_MASK;
	runstop = vdma_read(XAXIVDMA_TX_OFFSET, XAXIVDMA_CR_OFFSET) &
		XAXIVDMA_CR_RUNSTOP_MASK;

	printk("zz9000 vdma CR status: halted: %d run: %d\n", halted, runstop);
	printk("zz9000 vdma SR status: %x\n", vdma_read(XAXIVDMA_TX_OFFSET, XAXIVDMA_SR_OFFSET));
}

void zz9000_reinit_vdma(void) {
	if (!zz9000_vdma_hsize || !zz9000_vdma_vsize) {
		printk("zz9000_reinit_vdma(): error: hsize/vsize not set\n");
		return;
	}
	zz9000_setup_vdma(zz9000_vdma_hsize, zz9000_vdma_vsize, zz9000_vdma_hdiv, zz9000_vdma_vdiv);
}

void video_formatter_write(uint32_t data, uint16_t op) {
	mntzorro_write(3, data);
	mntzorro_write(2, 0x80000000 | op); // OP_MAX (vmax | hmax)
	mntzorro_write(2, 0x80000000); // NOP
	mntzorro_write(2, 0); // clear
	mntzorro_write(3, 0); // clear
}

void video_formatter_valign(void) {
	// vertical alignment
	mntzorro_write(3, 1);
	mntzorro_write(2, 0x80000000 + 0x5); // OP_VSYNC
	mntzorro_write(3, 0);
	mntzorro_write(2, 0x80000000); // NOP
	mntzorro_write(2, 0); // NOP
}

void video_formatter_init(int scalemode, int colormode, int width, int height,
		int htotal, int vtotal, int hss, int hse, int vss, int vse,
		int polarity) {
	video_formatter_write((vtotal << 16) | htotal, MNTVF_OP_MAX);
	video_formatter_write((height << 16) | width, MNTVF_OP_DIMENSIONS);
	video_formatter_write((hss << 16) | hse, MNTVF_OP_HS);
	video_formatter_write((vss << 16) | vse, MNTVF_OP_VS);
	video_formatter_write(polarity, MNTVF_OP_POLARITY);
	video_formatter_write(scalemode, MNTVF_OP_SCALE);
	video_formatter_write(colormode, MNTVF_OP_COLORMODE);
	video_formatter_valign();
}

void video_system_init(int hres, int vres, int htotal, int vtotal, int mhz,
		int vhz, int hdiv, int vdiv, int hdmi) {

	printk("zz9000: mode %d x %d [%d x %d] %d MHz %d Hz, hdiv: %d vdiv: %d\n", hres,
			vres, htotal, vtotal, mhz, vhz, hdiv, vdiv);

	zz9000_setup_clock(mhz);

	//printk("hdmi_set_video_mode()...\n");
	//hdmi_set_video_mode(hres, vres, mhz, vhz, hdmi);

	//printk("hdmi_ctrl_init()...\n");
	//hdmi_ctrl_init();

	zz9000_setup_vdma(hres, vres, hdiv, vdiv);
}

void video_mode_init(int mode, int scalemode, int colormode) {
	int hdiv = 1, vdiv = 1;
	struct zz_video_mode *vmode = &preset_video_modes[mode];

	if (scalemode & 1)
		hdiv = 2;
	if (scalemode & 2)
		vdiv = 2;

	if (colormode == 0)
		hdiv *= 4;
	if (colormode == 1)
		hdiv *= 2;

	video_formatter_init(scalemode, colormode,
			vmode->hres, vmode->vres,
			vmode->hmax, vmode->vmax,
			vmode->hstart, vmode->hend,
			vmode->vstart, vmode->vend,
			vmode->polarity);

	video_system_init(vmode->hres, vmode->vres, vmode->hmax,
			vmode->vmax, vmode->mhz, vmode->vhz,
			hdiv, vdiv, vmode->hdmi);
}

uint8_t zz9000_sprite_width	 = 16;
uint8_t zz9000_sprite_height = 16;
uint32_t zz9000_sprite_colors[4] = { 0x00ff00ff, 0x00000000, 0x00000000, 0x00000000 };

// TODO: make 32x48 template
uint8_t sprite_template[16*16];

void sprite_reset(void) {
	int y,x;
	for (y=0; y<48; y++) {
		for (x=0; x<32; x++) {
			uint32_t addr = y*32+x;
			uint32_t data = 0xff00ff;
			if (y<16 && x<16) {
				if (sprite_template[y*16+x]==1) {
					data = 0xffffffff;
				} else if (sprite_template[y*16+x]==2) {
					data = 0x000000;
				} else if (sprite_template[y*16+x]==3) {
					data = (255-15*y)<<16;
				}
			}
			video_formatter_write(addr, MNTVF_OP_SPRITE_ADDR);
			video_formatter_write(data, MNTVF_OP_SPRITE_DATA);
		}
	}

	video_formatter_write((1 << 16) | 1, MNTVF_OP_SPRITE_XY);

	printk("zz9000: sprite setup done!\n");
}

void update_hw_sprite(uint8_t *data, uint32_t *colors, uint16_t w, uint16_t h)
{
	uint8_t cur_bit = 0x80;
	uint8_t cur_color = 0, out_pos = 0, iter_offset = 0;
	uint8_t line_pitch = (w / 8) * 2;
	uint8_t cur_bytes[8];
	uint8_t y_line;

	for (y_line = 0; y_line < h; y_line++) {
		if (w <= 16) {
			cur_bytes[0] = data[y_line * line_pitch];
			cur_bytes[1] = data[(y_line * line_pitch) + 2];
			cur_bytes[2] = data[(y_line * line_pitch) + 1];
			cur_bytes[3] = data[(y_line * line_pitch) + 3];
		}
		else {
			cur_bytes[0] = data[y_line * line_pitch];
			cur_bytes[1] = data[(y_line * line_pitch) + 4];
			cur_bytes[2] = data[(y_line * line_pitch) + 1];
			cur_bytes[3] = data[(y_line * line_pitch) + 5];
			cur_bytes[4] = data[(y_line * line_pitch) + 2];
			cur_bytes[5] = data[(y_line * line_pitch) + 6];
			cur_bytes[6] = data[(y_line * line_pitch) + 3];
			cur_bytes[7] = data[(y_line * line_pitch) + 7];
		}

		while (out_pos < 8) {
			uint8_t i;
			for (i = 0; i < line_pitch; i += 2) {
				cur_color = (cur_bytes[i] & cur_bit) ? 1 : 0;
				if (cur_bytes[i + 1] & cur_bit) cur_color += 2;

				//sprite_buf[(y_line * 32) + out_pos + iter_offset] = colors[cur_color] & 0x00ffffff;

				video_formatter_write(((y_line * 32) + out_pos + iter_offset), MNTVF_OP_SPRITE_ADDR);
				video_formatter_write(colors[cur_color&3] & 0x00ffffff, MNTVF_OP_SPRITE_DATA);
				iter_offset += 8;
			}

			out_pos++;
			cur_bit >>= 1;
			iter_offset = 0;
		}
		cur_bit = 0x80;
		out_pos = 0;
	}
}

uint32_t zz9000_videomode = ZZVMODE_1024x768;
uint32_t zz9000_x1 = 0;
uint32_t zz9000_y1 = 0;
uint32_t zz9000_x2 = 100;
uint32_t zz9000_y2 = 100;
uint32_t zz9000_x3 = 0;
uint32_t zz9000_y3 = 0;
uint32_t zz9000_rgb1 = 0xff00ff;
uint32_t zz9000_rgb2 = 0;
uint32_t zz9000_colormode = 0;
uint32_t zz9000_blitter_colormode = 0;
uint32_t zz9000_blitter_src_pitch = 0;
uint32_t zz9000_blitter_src_offset = 0;
uint32_t zz9000_blitter_dst_pitch = 0;
uint32_t zz9000_blitter_dst_offset = 0;
uint32_t zz9000_blitter_user1 = 0;
uint32_t zz9000_blitter_user2 = 0;
uint32_t zz9000_blitter_user3 = 0;
uint32_t zz9000_blitter_user4 = 0;
uint32_t zz9000_sprite_x = 0;
uint32_t zz9000_sprite_y = 0;
uint32_t zz9000_pitch = 1024;
uint32_t fb_width = 1024;
uint32_t fb_height = 768;

static ssize_t zz9000_sysfs_get_mode(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", zz9000_videomode);
}
static ssize_t zz9000_sysfs_set_mode(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (!kstrtouint(buf, 10, &zz9000_videomode))
		video_mode_init(zz9000_videomode, 0, MNTVA_COLOR_32BIT);
	return count;
}
static ssize_t zz9000_sysfs_set_vdma(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	uint32_t val=0;
	if (!kstrtouint(buf, 16, &val)) {
		main_vdma = (uint32_t*)val;
	}
	return count;
}
static ssize_t zz9000_sysfs_get_vdma(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "0x%x\n", (uint32_t)main_vdma);
}
static ssize_t zz9000_sysfs_set_pitch(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &zz9000_pitch))
		return 0;
	return count;
}
static ssize_t zz9000_sysfs_get_x1(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", zz9000_x1);
}
static ssize_t zz9000_sysfs_set_x1(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &zz9000_x1))
		return 0;
	return count;
}
static ssize_t zz9000_sysfs_get_x2(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", zz9000_x2);
}
static ssize_t zz9000_sysfs_set_x2(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &zz9000_x2))
		return 0;
	return count;
}
static ssize_t zz9000_sysfs_get_y1(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", zz9000_y1);
}
static ssize_t zz9000_sysfs_set_y1(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &zz9000_y1))
		return 0;
	return count;
}
static ssize_t zz9000_sysfs_get_y2(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", zz9000_y2);
}
static ssize_t zz9000_sysfs_set_y2(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &zz9000_y2))
		return 0;
	return count;
}
static ssize_t zz9000_sysfs_get_rgb1(struct device *dev, struct device_attribute *attr, char *buf)
{
	return sprintf(buf, "0x%x\n", zz9000_rgb1);
}
static ssize_t zz9000_sysfs_set_rgb1(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &zz9000_rgb1))
		return 0;
	return count;
}

uint32_t pix_offset = 0;

static ssize_t zz9000_sysfs_set_pixels(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	memcpy((void*)((uint32_t)main_buffer_virt)+pix_offset, buf, count);
	pix_offset+=count;
	if (pix_offset>=fb_width*fb_height*4) {
		pix_offset = 0;
	}
	return count;
}
static ssize_t zz9000_sysfs_set_pixoffset(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (kstrtouint(buf, 10, &pix_offset))
		return 0;
	return count;
}

char zstate_debug = 0;

static ssize_t zz9000_sysfs_op(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	if (strstr(buf, "fillrect")) {
		fill_rect_solid(main_buffer_virt, zz9000_pitch, zz9000_x1,zz9000_y1,zz9000_x2-zz9000_x1,zz9000_y2-zz9000_y1,zz9000_rgb1,MNTVA_COLOR_32BIT);
	}
	else if (strstr(buf, "sprite")) {
		video_formatter_write((zz9000_y1 << 16) | zz9000_x1, MNTVF_OP_SPRITE_XY);
	}
	else if (strstr(buf, "zdebug")) {
		zstate_debug = 1-zstate_debug;
	}
	return count;
}

static DEVICE_ATTR(mode, 0644, zz9000_sysfs_get_mode, zz9000_sysfs_set_mode);
static DEVICE_ATTR(vdma, 0644, zz9000_sysfs_get_vdma, zz9000_sysfs_set_vdma);
static DEVICE_ATTR(pixels, 0200, NULL, zz9000_sysfs_set_pixels);
static DEVICE_ATTR(pitch, 0200, NULL, zz9000_sysfs_set_pitch);
static DEVICE_ATTR(x1, 0644, zz9000_sysfs_get_x1, zz9000_sysfs_set_x1);
static DEVICE_ATTR(y1, 0644, zz9000_sysfs_get_y1, zz9000_sysfs_set_y1);
static DEVICE_ATTR(x2, 0644, zz9000_sysfs_get_x2, zz9000_sysfs_set_x2);
static DEVICE_ATTR(y2, 0644, zz9000_sysfs_get_y2, zz9000_sysfs_set_y2);
static DEVICE_ATTR(rgb1, 0644, zz9000_sysfs_get_rgb1, zz9000_sysfs_set_rgb1);
static DEVICE_ATTR(pixoffset, 0200, NULL, zz9000_sysfs_set_pixoffset);
static DEVICE_ATTR(op, 0200, NULL, zz9000_sysfs_op);

#define MNT_FW_REVISION_MAJOR 2
#define MNT_FW_REVISION_MINOR 0

#define ZZTTY_READ_MAX 512
static uint8_t zztty_put_char = 0;
static volatile int zztty_write_pending = 0;

//int zz9000_zorro_thread_fn(void* private) {
static irqreturn_t zz9000_zorro_irq_fn(int irq, void *dev_id) {
	uint32_t z_state=0;
	uint32_t z_writereq=0;
	uint32_t z_readreq=0;
	uint32_t z_ds0, z_ds1, z_ds2, z_ds3;
	uint32_t z_data, z_addr;
	uint8_t need_req_ack = 0;
	uint8_t* ptr;

	//while (true) {
	if (true) {
		//	schedule();
		z_state = mntz[3];
		z_writereq = z_state & (1 << 31);
		z_readreq	 = z_state & (1 << 30);
		need_req_ack = 0;
		z_addr = mntz[0];

		//printk("ZSTATE %x ADDR %x W:%d R:%d\n",z_state,z_addr,z_writereq,z_readreq);

		if (z_writereq) {
			z_data = mntz[1];
			z_ds3 = z_state & (1 << 29);
			z_ds2 = z_state & (1 << 28);
			z_ds1 = z_state & (1 << 27);
			z_ds0 = z_state & (1 << 26);

			if (zstate_debug) {
				printk("ZWRT %x=%x\n",z_addr,z_data);
			}

			if (z_addr >= MNT_FB_BASE && z_addr<MNT_FB_BASE+(1024*1024*64-4)) {
				ptr = (uint8_t*)(((uint32_t)main_buffer_virt) + z_addr - MNT_FB_BASE);

				// TODO swap this in FPGA land
				if (z_ds3) ptr[0] = z_data >> 24;
				if (z_ds2) ptr[1] = z_data >> 16;
				if (z_ds1) ptr[2] = z_data >> 8;
				if (z_ds0) ptr[3] = z_data;
			} else if (z_addr >= MNT_REG_BASE && z_addr < MNT_FB_BASE) {

				// register area
				// convert 32bit to 16bit addresses
				if (z_ds3 && z_ds2) {
					z_data = z_data >> 16;
				} else if (z_ds1 && z_ds0) {
					z_data = z_data & 0xffff;
					z_addr += 2;
				} else {
					z_addr = 0; // cancel
				}

				switch (z_addr) {
				case MNT_REG_VMODE: {
					int mode = z_data & 0xff;
					int colormode = (z_data & 0xf00) >> 8;
					int scalemode = (z_data & 0xf000) >> 12;
					video_mode_init(mode, scalemode, colormode);
					zz9000_videomode = mode;
					zz9000_blitter_colormode = colormode; // FIXME
					break;
				}
				case MNT_REG_PAN_HI: {
					zz9000_pan_offset = z_data<<16;
					break;
				}
				case MNT_REG_PAN_LO: {
					zz9000_pan_offset |= z_data;
					printk("zz9000_pan_offset: %x\n", zz9000_pan_offset);
					zz9000_reinit_vdma();
					break;
				}
				case MNT_REG_SPRITEX:
					zz9000_sprite_x = (int16_t)z_data;
					break;
				case MNT_REG_SPRITEY:
					zz9000_sprite_y = (int16_t)z_data;
					video_formatter_write((zz9000_sprite_y << 16) | zz9000_sprite_x, MNTVF_OP_SPRITE_XY);
					break;
				case MNT_REG_X1:
					zz9000_x1 = z_data;
					break;
				case MNT_REG_Y1:
					zz9000_y1 = z_data;
					break;
				case MNT_REG_X2:
					zz9000_x2 = z_data;
					break;
				case MNT_REG_Y2:
					zz9000_y2 = z_data;
					break;
				case MNT_REG_BLITTER_SRC_HI:
					zz9000_blitter_src_offset = z_data << 16;
					break;
				case MNT_REG_BLITTER_SRC_LO:
					zz9000_blitter_src_offset |= z_data;
					break;
				case MNT_REG_BLITTER_DST_HI:
					zz9000_blitter_dst_offset = z_data << 16;
					break;
				case MNT_REG_BLITTER_DST_LO:
					zz9000_blitter_dst_offset |= z_data;
					break;
				case MNT_REG_BLITTER_COLORMODE:
					zz9000_blitter_colormode = z_data;
					break;
				case MNT_REG_BLITTER_SRC_PITCH:
					zz9000_blitter_src_pitch = z_data;
					break;
				case MNT_REG_BLITTER_DST_PITCH:
					zz9000_blitter_dst_pitch = z_data;
					break;
				case MNT_REG_BLITTER_USER1:
					zz9000_blitter_user1 = z_data;
					break;
				case MNT_REG_BLITTER_USER2:
					zz9000_blitter_user2 = z_data;
					break;
				case MNT_REG_BLITTER_USER3:
					zz9000_blitter_user3 = z_data;
					break;
				case MNT_REG_BLITTER_USER4:
					zz9000_blitter_user4 = z_data;
					break;
				case MNT_REG_X3:
					zz9000_x3 = z_data;
					break;
				case MNT_REG_Y3:
					zz9000_y3 = z_data;
					break;
				case MNT_REG_RGB1_HI:
					zz9000_rgb1 &= 0xffff0000;
					zz9000_rgb1 |= (((z_data & 0xff) << 8) | z_data >> 8);
					break;
				case MNT_REG_RGB1_LO:
					zz9000_rgb1 &= 0x0000ffff;
					zz9000_rgb1 |= (((z_data & 0xff) << 8) | z_data >> 8) << 16;
					break;
				case MNT_REG_RGB2_HI:
					zz9000_rgb2 &= 0xffff0000;
					zz9000_rgb2 |= (((z_data & 0xff) << 8) | z_data >> 8);
					break;
				case MNT_REG_RGB2_LO:
					zz9000_rgb2 &= 0x0000ffff;
					zz9000_rgb2 |= (((z_data & 0xff) << 8) | z_data >> 8) << 16;
					break;

				// RTG rendering
				case MNT_REG_OP_FILLRECT:
					//uint8_t mask = z_data; // FIXME

					fill_rect_solid(main_buffer_virt+zz9000_blitter_dst_offset, zz9000_blitter_dst_pitch, zz9000_x1, zz9000_y1, zz9000_x2, zz9000_y2, zz9000_rgb1, zz9000_blitter_colormode);
					break;

				case MNT_REG_OP_COPYRECT:
					{
						// copy rectangle
						uint32_t* fbdst = (uint32_t*) ((u32) main_buffer_virt + zz9000_blitter_dst_offset);
						uint32_t* fbsrc = (uint32_t*) ((u32) main_buffer_virt + zz9000_blitter_src_offset);
						uint32_t mask = (zz9000_blitter_colormode >> 8);

						switch (z_data) {
						case 1: // Regular BlitRect
							if (mask == 0xFF || (mask != 0xFF && (zz9000_blitter_colormode & 0x0F)) != MNTVA_COLOR_8BIT)
								copy_rect_nomask(fbdst, zz9000_blitter_dst_pitch,
																 zz9000_x1, zz9000_y1, zz9000_x2, zz9000_y2, zz9000_x3,
																 zz9000_y3, zz9000_blitter_colormode & 0x0F,
																 fbdst,
																 zz9000_blitter_dst_pitch, MINTERM_SRC);
							else
								copy_rect(fbdst, zz9000_blitter_dst_pitch,
													zz9000_x1, zz9000_y1, zz9000_x2, zz9000_y2, zz9000_x3,
													zz9000_y3, zz9000_blitter_colormode & 0x0F,
													fbdst,
													zz9000_blitter_dst_pitch, mask);
							break;
						case 2: // BlitRectNoMaskComplete
							copy_rect_nomask(fbdst, zz9000_blitter_dst_pitch,
															 zz9000_x1, zz9000_y1, zz9000_x2, zz9000_y2, zz9000_x3,
															 zz9000_y3, zz9000_blitter_colormode & 0x0F,
															 fbsrc,
															 zz9000_blitter_src_pitch, mask); // Mask in this case is minterm/opcode.
							break;
						}

						break;
					}

				case MNT_REG_OP_PATTERNRECT:
					{
						uint8_t draw_mode = zz9000_blitter_colormode >> 8;
						uint8_t* tmpl_data = (uint8_t*) ((u32) main_buffer_virt + zz9000_blitter_src_offset);
						uint32_t* fb = (uint32_t*) ((u32) main_buffer_virt + zz9000_blitter_dst_offset);
						uint16_t loop_rows = 0;
						uint32_t mask = z_data;
						uint8_t bpp = 2 * (zz9000_blitter_colormode & 0xff);
						if (bpp == 0) bpp = 1;


						if (z_data & 0x8000) {
							// pattern mode
							// TODO yoffset
							loop_rows = z_data & 0xff;
							mask = zz9000_blitter_user1;
							zz9000_blitter_src_pitch = 16;
							pattern_fill_rect(fb, zz9000_blitter_dst_pitch, (zz9000_blitter_colormode & 0x0F), zz9000_x1,
																zz9000_y1, zz9000_x2, zz9000_y2, draw_mode, mask,
																zz9000_rgb1, zz9000_rgb2, zz9000_x3, zz9000_y3, tmpl_data,
																zz9000_blitter_src_pitch, loop_rows);
						}
						else {
							template_fill_rect(fb, zz9000_blitter_dst_pitch, (zz9000_blitter_colormode & 0x0F), zz9000_x1,
																 zz9000_y1, zz9000_x2, zz9000_y2, draw_mode, mask,
																 zz9000_rgb1, zz9000_rgb2, zz9000_x3, zz9000_y3, tmpl_data,
																 zz9000_blitter_src_pitch);
						}
						break;
					}
				case MNT_REG_OP_INVERTRECT:
					{
						uint32_t* fb = (uint32_t*)((u32) main_buffer_virt + zz9000_blitter_dst_offset);

						invert_rect(fb, zz9000_blitter_dst_pitch, zz9000_x1, zz9000_y1, zz9000_x2, zz9000_y2,
												z_data & 0xFF, zz9000_blitter_colormode);
						break;
					}

				case MNT_REG_OP_P2CRECT:
					{
						// Rect P2C
						uint8_t draw_mode = zz9000_blitter_colormode >> 8;
						uint8_t planes = (z_data & 0xFF00) >> 8;
						uint8_t mask = (z_data & 0xFF);
						uint16_t num_rows = zz9000_blitter_user1;
						uint8_t layer_mask = zz9000_blitter_user2;
						uint8_t* bmp_data = (uint8_t*) ((u32) main_buffer_virt + zz9000_blitter_src_offset);
						uint32_t* fb = (uint32_t*)((u32) main_buffer_virt + zz9000_blitter_dst_offset);

						p2c_rect(fb, zz9000_blitter_dst_pitch,
										 zz9000_x1, 0, zz9000_x2, zz9000_y2, zz9000_x3,
										 zz9000_y3, num_rows, draw_mode, planes, mask,
										 layer_mask, zz9000_blitter_src_pitch, bmp_data);
						break;
					}

				case MNT_REG_OP_P2DRECT:
					{
						// Rect P2D
						uint8_t draw_mode = zz9000_blitter_colormode >> 8;
						uint8_t planes = (z_data & 0xFF00) >> 8;
						uint8_t mask = (z_data & 0xFF);
						uint16_t num_rows = zz9000_blitter_user1;
						uint8_t layer_mask = zz9000_blitter_user2;
						uint8_t* bmp_data = (uint8_t*) ((u32) main_buffer_virt + zz9000_blitter_src_offset);
						uint32_t* fb = (uint32_t*)((u32) main_buffer_virt + zz9000_blitter_dst_offset);

						p2d_rect(fb, zz9000_blitter_dst_pitch,
										 zz9000_x1, 0, zz9000_x2, zz9000_y2, zz9000_x3,
										 zz9000_y3, num_rows, draw_mode, planes, mask, layer_mask, zz9000_rgb1,
										 zz9000_blitter_src_pitch, bmp_data, (zz9000_blitter_colormode & 0x0F));
						break;
					}

				case MNT_REG_SPRITE_BITMAP:
					{
						uint8_t* bmp_data = (uint8_t*) (((u32)main_buffer_virt) + zz9000_blitter_src_offset);

						if (z_data == 1 || z_data == 2) break;

						//zz9000_sprite_x_offset = zz9000_x1;
						//zz9000_sprite_y_offset = zz9000_y1;
						uint32_t sprite_width	 = zz9000_x2;
						uint32_t sprite_height = zz9000_y2;

						update_hw_sprite(bmp_data, zz9000_sprite_colors, sprite_width, sprite_height);
						break;
					}
				case MNT_REG_SPRITE_COLORS:
					{
						if (z_data>3) z_data = 3;
						zz9000_sprite_colors[z_data] = (zz9000_blitter_user1 << 16) | zz9000_blitter_user2;
						if (zz9000_sprite_colors[z_data] == 0xff00ff) zz9000_sprite_colors[z_data] = 0xfe00fe;
						break;
					}
				case MNT_REG_TTY_PUT:
					{
						int i;
						//printk("zztty_put: %x\n",z_data&0xff);
						// FIXME hack
						for (i=0; i<zztty_open_count && i<1; i++) {
							tty_insert_flip_char(zztty_tty[i]->port, z_data&0xff, TTY_NORMAL);
							tty_flip_buffer_push(zztty_tty[i]->port);
						}
						break;
					}
				case MNT_REG_MOUSE_XY:
					{
						int x = (z_data&0xff);
						int y = ((z_data&0xff00)>>8);
						if (x>127) x=-(256-x);
						if (y>127) y=-(256-y);

						//printk("zz x: %d y: %d\n",x,y);
						input_report_rel(zzmouse_dev, REL_X, x);
						input_report_rel(zzmouse_dev, REL_Y, y);
						input_sync(zzmouse_dev);
						break;
					}
				case MNT_REG_MOUSE_BUTTONS:
					{
						printk("zz mbt %x\n",z_data);
						input_report_key(zzmouse_dev, BTN_LEFT, z_data&1);
						input_report_key(zzmouse_dev, BTN_RIGHT, (z_data&2)>>1);
						input_sync(zzmouse_dev);
						break;
					}
				case MNT_REG_KEYBOARD:
					{
						unsigned char scancode, down;

						scancode = z_data&0xff;
						down = !(scancode & 128);
						scancode &= 0x7f;

						printk("zz scan: %d down: %d mapped: %d\n",scancode,down,amiga_keymap[scancode]);

						if (scancode < 0x78) {		/* scancodes < 0x78 are keys */
							if (scancode == 98) {	/* CapsLock is a toggle switch key on Amiga */
								input_report_key(zzkbd_dev, amiga_keymap[scancode], 1);
								input_report_key(zzkbd_dev, amiga_keymap[scancode], 0);
							} else {
								input_report_key(zzkbd_dev, amiga_keymap[scancode], down);
							}

							input_sync(zzkbd_dev);
						}
					}
				}
			}
			// ack the write
			mntz[0] = (1 << 31);
			need_req_ack = 1;
		}
		else if (z_readreq) {

			if (z_addr >= MNT_FB_BASE && z_addr<MNT_FB_BASE+(1024*1024*64-4)) {
				// read from framebuffer / generic memory
				ptr = ((uint8_t*)main_buffer_virt) + z_addr - MNT_FB_BASE;

				// TODO: move swap to FPGA
				uint32_t b1 = ptr[0] << 24;
				uint32_t b2 = ptr[1] << 16;
				uint32_t b3 = ptr[2] << 8;
				uint32_t b4 = ptr[3];
				mntz[1] = b1 | b2 | b3 | b4;

			} else if (z_addr >= MNT_REG_BASE) {
				// read ARM "register"
				uint32_t data = 0;
				uint32_t z_addr32 = z_addr & 0xffffffc;

				if (z_addr32 == MNT_REG_FW_VERSION) {
					data = (MNT_FW_REVISION_MAJOR << 24 | MNT_FW_REVISION_MINOR << 16);
				} else if (z_addr32 == MNT_REG_USB_STATUS) {
					data = 0;
				} else if (z_addr32 == MNT_REG_USB_CAPACITY) {
					data = 0;
				} else if (z_addr32 == MNT_REG_TTY_GET) {
					if (!zztty_write_pending) {
						data = 0;
					} else {
						data = (zztty_put_char<<24)|(1<<16);
					}
					zztty_write_pending = 0;
				}

				mntz[1] = data;
			}
			// ack the read
			mntz[0] = (1 << 30);
			need_req_ack = 2;
		} else {
			// no request
		}

		if (need_req_ack) {
			while (1) {
				// 1. fpga needs to respond to flag bit 31 or 30 going high (signals request fulfilled)
				// 2. it does that by clearing the request bit
				// 3. we read register 3 until request bit (31:write, 30:read) goes to 0 again
				//
				uint32_t z_state = mntz[3];
				uint32_t z_writereq = (z_state & (1 << 31));
				uint32_t z_readreq = (z_state & (1 << 30));
				if (need_req_ack == 1 && !z_writereq)
					break;
				if (need_req_ack == 2 && !z_readreq)
					break;
				if ((z_state & 0xff) == 0)
					break; // reset
			}
			// acknowledge
			mntz[0] = 0;
			need_req_ack = 0;
		}
	}
	//return 0;
	return IRQ_RETVAL(1);
}

int zz9000_setup_threads(int irq) {
	int ret = request_irq(irq, zz9000_zorro_irq_fn, 0, "zz9000", 0);

	printk("ZZ9000 IRQ: %d\n",irq);

	if (ret < 0) {
		printk(KERN_ALERT "%s: request_irg failed with %d\n",
					 __func__, ret);
	}

	printk(KERN_ALERT "ZZ9000: IRQ %d registered!\n", irq);

	/*zorro_thread = kthread_create(zz9000_zorro_thread_fn, NULL, "zz9000_zorro");
	if (zorro_thread) {
		wake_up_process(zorro_thread);
	} else {
		return -1;
		}*/

	return 0;
}

static int zztty_carrier_raised(struct tty_port *port)
{
	return 1;
}

static struct tty_port zztty_port;
static const struct tty_port_operations zztty_port_ops =
	{
	 .carrier_raised = zztty_carrier_raised
	 //.dtr_rts
	};

static int zztty_open(struct tty_struct *tty, struct file *file)
{
	if (zztty_open_count>9) return -ENOMEM;

	//tty->driver_data = NULL;
	zztty_tty[zztty_open_count] = tty;
	zztty_open_count++;
	zztty_port.count++;
	zztty_port.tty = tty;
	tty_port_set_initialized(&zztty_port, 1);
	tty->port = &zztty_port;
	printk("zztty opened: %d\n",zztty_open_count);

	return 0;
}

static void zztty_close(struct tty_struct *tty, struct file *file)
{
	zztty_port.tty = NULL;
	zztty_port.count--;
	zztty_open_count--;
	zztty_tty[zztty_open_count] = NULL;
	printk("zztty closed: %d\n",zztty_open_count);
}

static void zztty_wait_until_sent(struct tty_struct *tty, int timeout);

// TODO semaphore
static int zztty_write(struct tty_struct *tty, const unsigned char *buffer, int count)
{
	if (count>0) {
		zztty_put_char = buffer[0];
		zztty_write_pending = 1;
		mntz[2] = (1<<30)|1; // raise amiga interrupt
		ndelay(50);
		mntz[2] = 1<<30|0; // lower amiga interrupt
		zztty_wait_until_sent(tty, 0);
		return 1;
	}

	/*for (i = 0; i < count; i++) {

		timeout=0;
		while (zztty_write_pending) {
			// busy wait...
			if (timeout>100000) {
				printk("zztty w timeout!\n");
				cancel = 1;
				break;
			}
			timeout++;
			if (!in_interrupt()) {
				schedule();
			}
			mntz[2] = (1<<30)|1; // raise amiga interrupt
			ndelay(10);
			mntz[2] = 1<<30|0; // lower amiga interrupt
			udelay(1);
		}

		if (!cancel) {
			zztty_put_char = buffer[i];
			zztty_write_pending = 1;

			// FIXME conflicts with video formatter!
			//printk("AIRQ1\n");
			mntz[2] = (1<<30)|1; // raise amiga interrupt
			ndelay(10);
			mntz[2] = 1<<30|0; // lower amiga interrupt
			//printk("AIRQ0\n");
		}

		if (cancel) break;
	}

	if (i>0) return i-cancel;*/
	return 0;
}

static int zztty_write_room(struct tty_struct *tty)
{
	//if (zztty_write_pending) return 0;
	return 32;
}

static void zztty_set_termios(struct tty_struct *tty, struct ktermios *old_termios)
{
}

static void zztty_wait_until_sent(struct tty_struct *tty, int timeout)
{
	unsigned long orig_jiffies;
	orig_jiffies = jiffies;
	if (!timeout) {
		timeout = msecs_to_jiffies(100);
	}

	printk("zztty_wait_until_sent %d\n",timeout);
	while (zztty_write_pending) {
		mntz[2] = (1<<30)|1; // raise amiga interrupt
		ndelay(50);
		mntz[2] = 1<<30|0; // lower amiga interrupt

		msleep_interruptible(1);
		if (signal_pending(current)) break;

		if (timeout && time_after(jiffies, orig_jiffies + timeout))
			break;
	}
}

static struct tty_operations zztty_ops = {
		.open = zztty_open,
		.close = zztty_close,
		.write = zztty_write,
		.write_room = zztty_write_room,
		.set_termios = zztty_set_termios,
		.wait_until_sent = zztty_wait_until_sent
};

static struct tty_driver* zztty_driver;

int zz9000_setup_tty(void) {
	int retval;

	zztty_driver = alloc_tty_driver(ZZTTY_MINORS);
	if (!zztty_driver)
		return -ENOMEM;

	zztty_driver->owner = THIS_MODULE;
	zztty_driver->driver_name = "zztty";
	zztty_driver->name = "zztty";
	//zztty_driver->devfs_name = "zztty%d";
	zztty_driver->major = 500; // FIXME
	zztty_driver->type = TTY_DRIVER_TYPE_SERIAL;
	zztty_driver->subtype = SERIAL_TYPE_NORMAL;
	zztty_driver->flags = TTY_DRIVER_REAL_RAW;
	zztty_driver->init_termios = tty_std_termios;
	zztty_driver->init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;
	tty_set_operations(zztty_driver, &zztty_ops);

	tty_port_init(&zztty_port);
	zztty_port.ops = &zztty_port_ops;
	tty_port_link_device(&zztty_port, zztty_driver, 0);

	retval = tty_register_driver(zztty_driver);

	if (retval) {
		printk(KERN_ERR "failed to register zztty_driver");
		put_tty_driver(zztty_driver);
		return retval;
	}
	return 0;
}

static void zzfb_destroy(struct fb_info *info)
{
	printk("zzfb_destroy()\n");
}

static int zzfb_mmap(struct fb_info *info, struct vm_area_struct *vma)
{
	return dma_mmap_coherent(info->dev, vma, info->screen_base,
													 info->fix.smem_start, info->fix.smem_len);
}

static const struct fb_ops zzfb_ops = {
	.owner		= THIS_MODULE,
	.fb_destroy	= zzfb_destroy,
	.fb_mmap = zzfb_mmap
};

static const struct fb_fix_screeninfo zzfb_fix = {
	.id		= "zz9000",
	.type		= FB_TYPE_PACKED_PIXELS,
	.visual		= FB_VISUAL_TRUECOLOR,
	.accel		= FB_ACCEL_NONE,
};

static const struct fb_var_screeninfo zzfb_var = {
	.height		= -1,
	.width		= -1,
	.activate	= FB_ACTIVATE_NOW,
	.vmode		= FB_VMODE_NONINTERLACED,
};

struct zzfb_par {
	u32 nothing_yet;
};

struct zzfb_params {
	u32 width;
	u32 height;
	u32 stride;
};

struct fb_info *zzfb_info;
struct zzfb_par *par;

// FIXME experimental, we put the linux fb at offset 32MB in the shared area
// so we can reach it via the pan register
#define ZZ_LINUX_FB_OFFSET 1024*1024*32

static int zz9000_setup_fb(void) {
	int ret;
	struct fb_bitfield bbf = {0,8,0};
	struct fb_bitfield rbf = {16,8,0};
	struct fb_bitfield gbf = {8,8,0};
	struct fb_bitfield xbf = {24,8,0};

	zzfb_info = framebuffer_alloc(sizeof(struct zzfb_par), dev);
	if (!zzfb_info)
		return -ENOMEM;

	par = zzfb_info->par;

	zzfb_info->fix = zzfb_fix;
	zzfb_info->fix.smem_start = (uint32_t)main_buffer+ZZ_LINUX_FB_OFFSET; // physical
	zzfb_info->fix.smem_len = 1024*1024*32; // FIXME
	zzfb_info->fix.line_length = fb_width*4;

	zzfb_info->var = zzfb_var;
	zzfb_info->var.xres = fb_width;
	zzfb_info->var.yres = fb_height;
	zzfb_info->var.xres_virtual = fb_width;
	zzfb_info->var.yres_virtual = fb_height;
	zzfb_info->var.bits_per_pixel = 32;
	zzfb_info->var.red = rbf;
	zzfb_info->var.green = gbf;
	zzfb_info->var.blue = bbf;
	zzfb_info->var.transp = xbf;
	zzfb_info->apertures = alloc_apertures(1);
	zzfb_info->apertures->ranges[0].base = zzfb_info->fix.smem_start;
	zzfb_info->apertures->ranges[0].size = zzfb_info->fix.smem_len;
	zzfb_info->fbops = &zzfb_ops;
	zzfb_info->flags = FBINFO_DEFAULT | FBINFO_MISC_FIRMWARE;
	zzfb_info->screen_base = (char*)main_buffer_virt; // virtual

	dev_info(dev, "zz9000 framebuffer at 0x%lx, 0x%x bytes, mapped to 0x%x\n",
					 zzfb_info->fix.smem_start, zzfb_info->fix.smem_len,
					 (uint32_t)zzfb_info->screen_base);
	dev_info(dev, "zz9000 mode=%dx%dx%d, linelength=%d\n",
					 zzfb_info->var.xres, zzfb_info->var.yres,
					 zzfb_info->var.bits_per_pixel, zzfb_info->fix.line_length);

	ret = register_framebuffer(zzfb_info);
	if (ret < 0) {
		dev_err(dev, "Unable to register zzfb: %d\n", ret);
		return ret;
	}
	printk("zz9000_setup_fb() done.\n");
	return 0;
}

static int zz9000_setup_keyboard_and_mouse(void)
{
	int i, error;

	zzkbd_dev = input_allocate_device();
	if (!zzkbd_dev) return -ENOMEM;

	zzkbd_dev->name = "zzkbd";
	zzkbd_dev->phys = "zzkbd/input0";
	zzkbd_dev->id.bustype = BUS_AMIGA;
	zzkbd_dev->id.vendor = 0x6d6e;
	zzkbd_dev->id.product = 0x0004;
	zzkbd_dev->id.version = 0x0100;

	zzkbd_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_REP);
	for (i = 0; i < ARRAY_SIZE(amiga_keymap); i++) {
		input_set_capability(zzkbd_dev, EV_KEY, amiga_keymap[i]);
	}

	error = input_register_device(zzkbd_dev);
	if (error) return -ENODEV;

	zzmouse_dev = input_allocate_device();
	if (!zzmouse_dev) return -ENOMEM;

	zzmouse_dev->name = "zzmouse";
	zzmouse_dev->phys = "zzmouse/input0";
	zzmouse_dev->id.bustype = BUS_AMIGA;
	zzmouse_dev->id.vendor = 0x6d6e;
	zzmouse_dev->id.product = 0x0004;
	zzmouse_dev->id.version = 0x0100;
	zzmouse_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_REL);
	zzmouse_dev->keybit[BIT_WORD(BTN_MOUSE)] = BIT_MASK(BTN_LEFT) | BIT_MASK(BTN_RIGHT);
	zzmouse_dev->relbit[0] = BIT_MASK(REL_X) | BIT_MASK(REL_Y);

	error = input_register_device(zzmouse_dev);
	if (error) return -ENODEV;

	return 0;
}

static struct i2c_board_info sii_info = {
			I2C_BOARD_INFO("sii9021", 0x3b),
};

static int zz9000_i2c_init(void) {
	struct i2c_adapter *i2c_adap;
	struct i2c_client *sii;

	i2c_adap = i2c_get_adapter(0);
	printk("i2c_get_adapter(0) passed: %x\n",(uint32_t)i2c_adap);

	if (i2c_adap == NULL) {
		return -EPROBE_DEFER;
	}

	sii = i2c_new_client_device(i2c_adap, &sii_info);
	printk("i2c_new_client_device() passed: %x\n",(uint32_t)sii);
	i2c_smbus_write_byte_data(sii, 0xc7, 0);
	i2c_smbus_write_byte_data(sii, 0x1e, 0);
	i2c_smbus_write_byte_data(sii, 0x09, 0);
	i2c_smbus_write_byte_data(sii, 0x0a, 0);
	i2c_smbus_write_byte_data(sii, 0x60, 4);
	i2c_smbus_write_byte_data(sii, 0x3c, 1);
	i2c_smbus_write_byte_data(sii, 0x1a, 0x10);
	i2c_smbus_write_byte_data(sii, 0x00, 0x4c);
	i2c_smbus_write_byte_data(sii, 0x01, 0x1d);
	i2c_smbus_write_byte_data(sii, 0x02, 0x70);
	i2c_smbus_write_byte_data(sii, 0x03, 0x17);
	i2c_smbus_write_byte_data(sii, 0x03, 0x70);
	i2c_smbus_write_byte_data(sii, 0x03, 0x06);
	i2c_smbus_write_byte_data(sii, 0x03, 0xee);
	i2c_smbus_write_byte_data(sii, 0x03, 0x02);
	i2c_smbus_write_byte_data(sii, 0x03, 0x70);
	i2c_smbus_write_byte_data(sii, 0x1a, 0x00);
	printk("zz9000_i2c_init() done.\n");

	return 0;
};

static int zz9000_probe(struct platform_device *ofdev)
{
	int irq;
	struct resource *res;
	int i2c_res;

	printk("zz9000_probe()\n");
	i2c_res = zz9000_i2c_init();
	if (i2c_res) {
		printk("zz9000_probe() deferring...\n");
		return i2c_res;
	}

	res = platform_get_resource(ofdev, IORESOURCE_IRQ, 0);
	if (!res) {
		printk(KERN_ALERT "ZZ9000: could not get platform IRQ resource.\n");
	}

	dev = kzalloc(sizeof(struct device), GFP_KERNEL);
	dev_set_name(dev, "zz9000");
	dev->release = zz9000_release;

	if (device_register(dev) < 0) {
		dev_err(dev, "device_register() failed.");
		kfree(dev);
		return -ENODEV;
	}

	mntz = (uint32_t*)ioremap(MNTZ_BASE, 0x1000);
	zz9000_dump_status();

	vdma = (uint32_t*)ioremap(XPAR_VIDEO_AXI_VDMA_0_BASEADDR, 0x1000);
	clkw = (uint32_t*)ioremap(XPAR_CLK_WIZ_0_BASEADDR, 0x1000);

	zz9000_allocate_buffer();
	video_mode_init(ZZVMODE_1024x768, 0, MNTVA_COLOR_32BIT);
	sprite_reset();

	device_create_file(dev, &dev_attr_mode);
	device_create_file(dev, &dev_attr_vdma);
	device_create_file(dev, &dev_attr_pitch);
	device_create_file(dev, &dev_attr_x1);
	device_create_file(dev, &dev_attr_y1);
	device_create_file(dev, &dev_attr_x2);
	device_create_file(dev, &dev_attr_y2);
	device_create_file(dev, &dev_attr_rgb1);
	device_create_file(dev, &dev_attr_op);
	//device_create_file(dev, &dev_attr_pixels);
	device_create_file(dev, &dev_attr_pixoffset);

	zz9000_setup_fb();

	zz9000_setup_threads(res->start);
	zz9000_setup_tty();
	zz9000_setup_keyboard_and_mouse();

	printk("zz9000_setup_xxx() done\n");
	return 0;
}

static int zz9000_remove(struct platform_device* ofdev)
{
	if (zzfb_info) {
		unregister_framebuffer(zzfb_info);
		framebuffer_release(zzfb_info);
	}

	iounmap(mntz);
	iounmap(vdma);
	iounmap(clkw);
	//uio_unregister_device(info);
	device_unregister(dev);
	//kfree(info);
	kfree(dev);

	return 0;
}

//module_init(zz9000_probe);
//module_exit(zz9000_remove);

static const struct of_device_id zz_of_match[] = {
	 { .compatible = "mnt,zz9000", },
	 { /* end of list */ },
};
MODULE_DEVICE_TABLE(of, zz_of_match);

static struct platform_driver zz_of_driver = {
	 .probe			 = zz9000_probe,
	 .remove		 = zz9000_remove,
	 .driver = {
			.name = "zz9000",
			.owner = THIS_MODULE,
			.of_match_table = zz_of_match,
	 },
};

module_platform_driver(zz_of_driver);

MODULE_AUTHOR("Lukas F. Hartmann");
MODULE_LICENSE("GPL");
