#include <linux/kernel.h>
#include "zz9000_gfx.h"

void fill_rect_solid(uint32_t* fb, uint32_t fb_pitch, uint16_t rect_x1, uint16_t rect_y1, uint16_t w, uint16_t h, uint32_t rect_rgb, uint32_t color_format)
{
	uint32_t* p = fb + (rect_y1 * fb_pitch);
	uint16_t* p16;
	uint16_t rect_y2 = rect_y1 + h, rect_x2 = rect_x1 + w;
	uint16_t x;
  uint16_t cur_y;

	for (cur_y = rect_y1; cur_y < rect_y2; cur_y++) {
		switch(color_format) {
			case MNTVA_COLOR_8BIT:
				memset((uint8_t *)p + rect_x1, (uint8_t)(rect_rgb >> 24), w);
				break;
			case MNTVA_COLOR_16BIT565:
				x = rect_x1;
				p16 = (uint16_t *)p;
				while(x < rect_x2) {
					p16[x++] = rect_rgb;
				}
				break;
			case MNTVA_COLOR_32BIT:
				x = rect_x1;
				while(x < rect_x2) {
					p[x++] = rect_rgb;
				}
				break;
			default:
				// Unknown/unhandled color format.
				break;
		}
		p += fb_pitch;
	}
}

void invert_rect(uint32_t* fb, uint32_t fb_pitch, uint16_t rect_x1, uint16_t rect_y1, uint16_t w, uint16_t h, uint8_t mask, uint32_t color_format)
{
	uint32_t* dp = fb + (rect_y1 * fb_pitch);
	uint16_t x;

	uint16_t rect_y2 = rect_y1 + h, rect_x2 = rect_x1 + w;

  uint16_t cur_y;
	for (cur_y = rect_y1; cur_y < rect_y2; cur_y++) {
		x = rect_x1;
		while (x < rect_x2) {
			INVERT_PIXEL;
			x++;
		}
		dp += fb_pitch;
	}
}


#define PATTERN_FILLRECT_LOOPX \
	tmpl_x ^= 0x01; \
	cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

#define PATTERN_FILLRECT_LOOPY \
	tmpl_data += 2 ; \
	if ((y_line + y_offset + 1) % loop_rows == 0) \
		tmpl_data = tmpl_base; \
	tmpl_x = tmpl_x_base; \
	cur_bit = base_bit; \
	dp += fb_pitch / 4;

void pattern_fill_rect(uint32_t* fb, uint32_t fb_pitch, uint32_t color_format, uint16_t rect_x1, uint16_t rect_y1, uint16_t w, uint16_t h,
	uint8_t draw_mode, uint8_t mask, uint32_t fg_color, uint32_t bg_color,
	uint16_t x_offset, uint16_t y_offset,
	uint8_t *tmpl_data, uint16_t tmpl_pitch, uint16_t loop_rows)
{
	uint32_t rect_x2 = rect_x1 + w;
	uint32_t *dp = fb + (rect_y1 * (fb_pitch / 4));
	uint8_t* tmpl_base = tmpl_data;

	uint16_t tmpl_x, tmpl_x_base;

	uint8_t cur_bit, base_bit, inversion = 0;
	uint8_t u8_fg = fg_color >> 24;
	uint8_t u8_bg = bg_color >> 24;
	uint8_t cur_byte = 0;

	tmpl_x = (x_offset / 8) % 2;
	tmpl_data += (y_offset % loop_rows) * 2;
	tmpl_x_base = tmpl_x;

	cur_bit = base_bit = (0x80 >> (x_offset % 8));

	if (draw_mode & INVERSVID) inversion = 1;
	draw_mode &= 0x03;

	if (draw_mode == JAM1) {
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			uint16_t x = rect_x1;

			cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

			while (x < rect_x2) {
				if (cur_bit == 0x80 && x < rect_x2 - 8) {
					if (mask == 0xFF) {
						SET_FG_PIXELS;
					}
					else {
						SET_FG_PIXELS_MASK;
					}
					x += 8;
				}
				else {
					while (cur_bit > 0 && x < rect_x2) {
						if (cur_byte & cur_bit) {
							SET_FG_PIXEL_MASK;
						}
						x++;
						cur_bit >>= 1;
					}
					cur_bit = 0x80;
				}
				PATTERN_FILLRECT_LOOPX;
			}
			PATTERN_FILLRECT_LOOPY;
		}

		return;
	}
	else if (draw_mode == JAM2) {
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			uint16_t x = rect_x1;

			cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

			while (x < rect_x2) {
				if (cur_bit == 0x80 && x < rect_x2 - 8) {
					if (mask == 0xFF) {
						SET_FG_OR_BG_PIXELS;
					}
					else {
						SET_FG_OR_BG_PIXELS_MASK;
					}
					x += 8;
				}
				else {
					while (cur_bit > 0 && x < rect_x2) {
						if (cur_byte & cur_bit) {
							SET_FG_PIXEL_MASK;
						}
						else {
							SET_BG_PIXEL_MASK;
						}
						x++;
						cur_bit >>= 1;
					}
					cur_bit = 0x80;
				}
				PATTERN_FILLRECT_LOOPX;
			}
			PATTERN_FILLRECT_LOOPY;
		}

		return;
	}
	else { // COMPLEMENT
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			uint16_t x = rect_x1;

			cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

			while (x < rect_x2) {
				if (cur_bit == 0x80 && x < rect_x2 - 8) {
					INVERT_PIXELS;
					x += 8;
				}
				else {
					while (cur_bit > 0 && x < rect_x2) {
						if (cur_byte & cur_bit) {
							INVERT_PIXEL;
						}
						x++;
						cur_bit >>= 1;
					}
					cur_bit = 0x80;
				}
				PATTERN_FILLRECT_LOOPX;
			}
			PATTERN_FILLRECT_LOOPY;	
		}
	}
}

void template_fill_rect(uint32_t* fb, uint32_t fb_pitch, uint32_t color_format, uint16_t rect_x1, uint16_t rect_y1, uint16_t w, uint16_t h,
	uint8_t draw_mode, uint8_t mask, uint32_t fg_color, uint32_t bg_color,
	uint16_t x_offset, uint16_t y_offset,
	uint8_t *tmpl_data, uint16_t tmpl_pitch)
{
	uint32_t rect_x2 = rect_x1 + w;
	uint32_t *dp = fb + (rect_y1 * (fb_pitch / 4));

	uint16_t tmpl_x, tmpl_x_base;

	uint8_t cur_bit, base_bit, inversion = 0;
	uint8_t u8_fg = fg_color >> 24;
	uint8_t u8_bg = bg_color >> 24;
	uint8_t cur_byte = 0;

	tmpl_x = x_offset / 8;
	tmpl_x_base = tmpl_x;

	cur_bit = base_bit = (0x80 >> (x_offset % 8));

	if (draw_mode & INVERSVID) inversion = 1;
	draw_mode &= 0x03;

	if (draw_mode == JAM1) {
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			uint16_t x = rect_x1;

			cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

			while (x < rect_x2) {
				if (cur_bit == 0x80 && x < rect_x2 - 8) {
					if (mask == 0xFF) {
						SET_FG_PIXELS;
					}
					else {
						SET_FG_PIXELS_MASK;
					}
					x += 8;
				}
				else {
					while (cur_bit > 0 && x < rect_x2) {
						if (cur_byte & cur_bit) {
							SET_FG_PIXEL_MASK;
						}
						x++;
						cur_bit >>= 1;
					}
					cur_bit = 0x80;
				}
				TEMPLATE_FILLRECT_LOOPX;
			}
			TEMPLATE_FILLRECT_LOOPY;
		}

		return;
	}
	else if (draw_mode == JAM2) {
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			uint16_t x = rect_x1;

			cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

			while (x < rect_x2) {
				if (cur_bit == 0x80 && x < rect_x2 - 8) {
					if (mask == 0xFF) {
						SET_FG_OR_BG_PIXELS;
					}
					else {
						SET_FG_OR_BG_PIXELS_MASK;
					}
					x += 8;
				}
				else {
					while (cur_bit > 0 && x < rect_x2) {
						if (cur_byte & cur_bit) {
							SET_FG_PIXEL_MASK;
						}
						else {
							SET_BG_PIXEL_MASK;
						}
						x++;
						cur_bit >>= 1;
					}
					cur_bit = 0x80;
				}
				TEMPLATE_FILLRECT_LOOPX;
			}
			TEMPLATE_FILLRECT_LOOPY;
		}

		return;
	}
	else { // COMPLEMENT
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			uint16_t x = rect_x1;

			cur_byte = (inversion) ? tmpl_data[tmpl_x] ^ 0xFF : tmpl_data[tmpl_x];

			while (x < rect_x2) {
				if (cur_bit == 0x80 && x < rect_x2 - 8) {
					INVERT_PIXELS;
					x += 8;
				}
				else {
					while (cur_bit > 0 && x < rect_x2) {
						if (cur_byte & cur_bit) {
							INVERT_PIXEL;
						}
						x++;
						cur_bit >>= 1;
					}
					cur_bit = 0x80;
				}
				TEMPLATE_FILLRECT_LOOPX;
			}
			TEMPLATE_FILLRECT_LOOPY;	
		}
	}
}

void fill_template(uint32_t* fb, uint32_t fb_pitch, uint32_t bpp, uint16_t rect_x1, uint16_t rect_y1, uint16_t rect_x2, uint16_t rect_y2,
		uint8_t draw_mode, uint8_t mask, uint32_t fg_color, uint32_t bg_color, uint16_t x_offset, uint16_t y_offset, uint8_t* tmpl_data, uint16_t tmpl_pitch, uint16_t loop_rows)
{
	uint8_t inversion = 0;
	uint16_t rows;
	int bitoffset;
	uint8_t* dp=(uint8_t*)(fb);
	uint8_t* tmpl_base;

	uint16_t width = rect_x2-rect_x1+1;

	if (draw_mode & INVERSVID) inversion = 1;
	draw_mode &= 0x03;

	bitoffset = x_offset % 8;
	tmpl_base = tmpl_data + x_offset / 8;

	// starting position in destination
	dp += rect_y1*fb_pitch + rect_x1*bpp;

	// number of 8-bit blocks of source
	uint16_t loop_x = x_offset;
	uint16_t loop_y = y_offset;

	for (rows = rect_y1; rows <= rect_y2; rows++, dp += fb_pitch, tmpl_base += tmpl_pitch) {
		unsigned long cols;
		uint8_t* dp2 = dp;
		uint8_t* tmpl_mem;
		unsigned int data;

		tmpl_mem = tmpl_base;
		data = *tmpl_mem;

		for (cols = 0; cols < width; cols += 8, dp2 += bpp*8) {
			unsigned int byte;
			long bits;
			long max = width - cols;

			if (max > 8) max = 8;

			// loop through 16-bit horizontal pattern
			if (loop_rows>0) {
				tmpl_mem = tmpl_data+(loop_y%loop_rows)*2;
				byte = tmpl_mem[loop_x%2];
				loop_x++;
			} else {
				data <<= 8;
				data |= *++tmpl_mem;
				byte = data >> (8 - bitoffset);
			}

			switch (draw_mode)
			{
				case JAM1:
				{
					for (bits = 0; bits < max; bits++) {
						int bit_set = (byte & 0x80);
						byte <<= 1;
						if (inversion) bit_set = !bit_set;

						if (bit_set) {

							if (bpp == 1) {
								dp2[bits] = fg_color>>24;
							} else if (bpp == 2) {
								((uint16_t*)dp2)[bits] = fg_color;
							} else if (bpp == 4) {
								((uint32_t*)dp2)[bits] = fg_color;
							}

							// TODO mask
							//dp2[bits] = (fg_color & mask) | (dp2[bits] & ~mask);
						}
					}
					break;
				}
				case JAM2:
				{
					for (bits = 0; bits < max; bits++) {
						char bit_set = (byte & 0x80);
						byte <<= 1;
						if (inversion) bit_set = !bit_set;

						uint32_t color = bit_set ? fg_color : bg_color;
						//if (bit_set) printf("#");
						//else printf(".");

						if (bpp == 1) {
							dp2[bits] = color>>24;
						} else if (bpp == 2) {
							((uint16_t*)dp2)[bits] = color;
						} else if (bpp == 4) {
							((uint32_t*)dp2)[bits] = color;
						}

						// NYI
						//	dp2[bits] = (color & mask) | (dp2[bits] & ~mask);
					}
					break;
				}
				case COMPLEMENT:
				{
					for (bits = 0; bits < max; bits++) {
						int bit_set = (byte & 0x80);
						byte <<= 1;
						if (bit_set) {
							if (bpp == 1) {
								dp2[bits] ^= 0xff;
							} else if (bpp == 2) {
								((uint16_t*)dp2)[bits] ^= 0xffff;
							} else if (bpp == 4) {
								((uint32_t*)dp2)[bits] ^= 0xffffffff;
							}
						}
						// TODO mask
					}
					break;
				}
			}
		}
		loop_y++;
	}
}

void copy_rect_nomask(uint32_t* fb, uint32_t fb_pitch, uint16_t rect_x1, uint16_t rect_y1, uint16_t w, uint16_t h, uint16_t rect_sx, uint16_t rect_sy, uint32_t color_format, uint32_t* sp_src, uint32_t src_pitch, uint8_t draw_mode)
{
	uint32_t* dp = fb + (rect_y1 * fb_pitch);
	uint32_t* sp = sp_src + (rect_sy * src_pitch);
	uint16_t rect_y2 = rect_y1 + h - 1;
	uint8_t mask = 0xFF; // Perform mask handling, just in case we get a FillRectComplete at some point.
	uint32_t color_mask = 0x00FFFFFF;

	uint8_t u8_fg = 0;
	uint32_t fg_color = 0;

	int32_t line_step_d = fb_pitch, line_step_s = src_pitch;
	int8_t x_reverse = 0;

	if (rect_sy < rect_y1) {
		line_step_d = -fb_pitch;
		dp = fb + (rect_y2 * fb_pitch);
		line_step_s = -src_pitch;
		sp = sp_src + ((rect_sy + h - 1) * src_pitch);
	}

	if (rect_sx < rect_x1) {
		x_reverse = 1;
	}

	if (draw_mode == MINTERM_SRC) {
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			switch(color_format) {
				case MNTVA_COLOR_8BIT:
					if (!x_reverse)
						memcpy((uint8_t *)dp + rect_x1, (uint8_t *)sp + rect_sx, w);
					else
						memmove((uint8_t *)dp + rect_x1, (uint8_t *)sp + rect_sx, w);
					break;
				case MNTVA_COLOR_16BIT565:
					if (!x_reverse)
						memcpy((uint16_t *)dp + rect_x1, (uint16_t *)sp + rect_sx, w * 2);
					else
						memmove((uint16_t *)dp + rect_x1, (uint16_t *)sp + rect_sx, w * 2);
					break;
				case MNTVA_COLOR_32BIT:
					if (!x_reverse)
						memcpy(dp + rect_x1, sp + rect_sx, w * 4);
					else
						memmove(dp + rect_x1, sp + rect_sx, w * 4);
					break;
			}
			dp += line_step_d;
			sp += line_step_s;
		}
	}
	else {
    uint16_t y_line;
		for (y_line = 0; y_line < h; y_line++) {
			if (x_reverse) {
        int16_t x;
				for (x = w; x >= 0; x--) {
					if (color_format == MNTVA_COLOR_8BIT) {
						u8_fg = ((uint8_t *)sp)[rect_sx + x];
						HANDLE_MINTERM_PIXEL_8(u8_fg, ((uint8_t *)dp)[rect_x1 + x]);
					}
					else {
						if (color_format == MNTVA_COLOR_16BIT565)
							fg_color = ((uint16_t *)sp)[rect_sx + x];
						else
							fg_color = sp[rect_sx + x];
						HANDLE_MINTERM_PIXEL_16_32(fg_color, dp);
					}
				}
			}
			else {
        int16_t x;
				for (x = 0; x < w; x++) {
					if (color_format == MNTVA_COLOR_8BIT) {
						u8_fg = ((uint8_t *)sp)[rect_sx + x];
						HANDLE_MINTERM_PIXEL_8(u8_fg, ((uint8_t *)dp)[rect_x1 + x]);
					}
					else {
						if (color_format == MNTVA_COLOR_16BIT565)
							fg_color = ((uint16_t *)sp)[rect_sx + x];
						else
							fg_color = sp[rect_sx + x];
						HANDLE_MINTERM_PIXEL_16_32(fg_color, dp);
					}
				}
			}
			dp += line_step_d;
			sp += line_step_s;
		}
	}
}

void copy_rect(uint32_t* fb, uint32_t fb_pitch, uint16_t rect_x1, uint16_t rect_y1, uint16_t w, uint16_t h, uint16_t rect_sx, uint16_t rect_sy, uint32_t color_format, uint32_t* sp_src, uint32_t src_pitch, uint8_t mask)
{
	uint32_t* dp = fb + (rect_y1 * fb_pitch);
	uint32_t* sp = sp_src + (rect_sy * src_pitch);
	uint16_t rect_y2 = rect_y1 + h - 1;//, rect_x2 = rect_x1 + h - 1;

	int32_t line_step_d = fb_pitch, line_step_s = src_pitch;
	int8_t x_reverse = 0;
  uint16_t y_line;
  int16_t x;

	if (rect_sy < rect_y1) {
		line_step_d = -fb_pitch;
		dp = fb + (rect_y2 * fb_pitch);
		line_step_s = -src_pitch;
		sp = sp_src + ((rect_sy + h - 1) * src_pitch);
	}

	if (rect_sx < rect_x1) {
		x_reverse = 1;
	}

	for (y_line = 0; y_line < h; y_line++) {
		if (x_reverse) {
			for (x = w; x >= 0; x--) {
				((uint8_t *)dp)[rect_x1 + x] = (((uint8_t *)dp)[rect_x1 + x] & (mask ^ 0xFF)) | (((uint8_t *)sp)[rect_sx + x] & mask);
			}
		}
		else {
			for (x = 0; x < w; x++) {
				((uint8_t *)dp)[rect_x1 + x] = (((uint8_t *)dp)[rect_x1 + x] & (mask ^ 0xFF)) | (((uint8_t *)sp)[rect_sx + x] & mask);
			}
		}
		dp += line_step_d;
		sp += line_step_s;
	}
}

void draw_line_solid(uint32_t* fb, uint32_t fb_pitch, int16_t rect_x1, int16_t rect_y1, int16_t rect_x2, int16_t rect_y2, uint16_t len,
	uint32_t fg_color, uint32_t color_format)
{
	int16_t x1 = rect_x1, y1 = rect_y1;
	int16_t x2 = rect_x1 + rect_x2, y2 = rect_y1 + rect_y2;

	uint8_t u8_fg = fg_color >> 24;

	uint32_t* dp = fb + (y1 * fb_pitch);
	int32_t line_step = fb_pitch;
	int8_t x_reverse = 0;

	int16_t dx, dy, dx_abs, dy_abs, ix, iy, x = x1;
  uint16_t i;

	if (x2 < x1)
		x_reverse = 1;
	if (y2 < y1)
		line_step = -fb_pitch;

	dx = x2 - x1;
	dy = y2 - y1;
	dx_abs = abs(dx);
	dy_abs = abs(dy);
	ix = dy_abs >> 1;
	iy = dx_abs >> 1;

	SET_FG_PIXEL;

	if (dx_abs >= dy_abs) {
		if (!len) len = dx_abs;
		for (i = 0; i < len; i++) {
			iy += dy_abs;
			if (iy >= dx_abs) {
				iy -= dx_abs;
				dp += line_step;
			}
			x += (x_reverse) ? -1 : 1;

			SET_FG_PIXEL;
		}
	}
	else {
		if (!len) len = dy_abs;
		for (i = 0; i < len; i++) {
			ix += dx_abs;
			if (ix >= dy_abs) {
				ix -= dy_abs;
				x += (x_reverse) ? -1 : 1;
			}
			dp += line_step;

			SET_FG_PIXEL;
		}
	}
}

#define DECODE_PLANAR_PIXEL(a) \
	switch (planes) { \
		case 8: if (layer_mask & 0x80 && bmp_data[(plane_size * 7) + cur_byte] & cur_bit) a |= 0x80; \
		case 7: if (layer_mask & 0x40 && bmp_data[(plane_size * 6) + cur_byte] & cur_bit) a |= 0x40; \
		case 6: if (layer_mask & 0x20 && bmp_data[(plane_size * 5) + cur_byte] & cur_bit) a |= 0x20; \
		case 5: if (layer_mask & 0x10 && bmp_data[(plane_size * 4) + cur_byte] & cur_bit) a |= 0x10; \
		case 4: if (layer_mask & 0x08 && bmp_data[(plane_size * 3) + cur_byte] & cur_bit) a |= 0x08; \
		case 3: if (layer_mask & 0x04 && bmp_data[(plane_size * 2) + cur_byte] & cur_bit) a |= 0x04; \
		case 2: if (layer_mask & 0x02 && bmp_data[plane_size + cur_byte] & cur_bit) a |= 0x02; \
		case 1: if (layer_mask & 0x01 && bmp_data[cur_byte] & cur_bit) a |= 0x01; \
			break; \
	}

#define DECODE_INVERTED_PLANAR_PIXEL(a) \
	switch (planes) { \
		case 8: if (layer_mask & 0x80 && (bmp_data[(plane_size * 7) + cur_byte] ^ 0xFF) & cur_bit) a |= 0x80; \
		case 7: if (layer_mask & 0x40 && (bmp_data[(plane_size * 6) + cur_byte] ^ 0xFF) & cur_bit) a |= 0x40; \
		case 6: if (layer_mask & 0x20 && (bmp_data[(plane_size * 5) + cur_byte] ^ 0xFF) & cur_bit) a |= 0x20; \
		case 5: if (layer_mask & 0x10 && (bmp_data[(plane_size * 4) + cur_byte] ^ 0xFF) & cur_bit) a |= 0x10; \
		case 4: if (layer_mask & 0x08 && (bmp_data[(plane_size * 3) + cur_byte] ^ 0xFF) & cur_bit) a |= 0x08; \
		case 3: if (layer_mask & 0x04 && (bmp_data[(plane_size * 2) + cur_byte] ^ 0xFF) & cur_bit) a |= 0x04; \
		case 2: if (layer_mask & 0x02 && (bmp_data[plane_size + cur_byte] ^ 0xFF) & cur_bit) a |= 0x02; \
		case 1: if (layer_mask & 0x01 && (bmp_data[cur_byte] ^ 0xFF) & cur_bit) a |= 0x01; \
			break; \
	}

void p2c_rect(uint32_t* fb, uint32_t fb_pitch, int16_t sx, int16_t sy, int16_t dx, int16_t dy, int16_t w, int16_t h, uint16_t sh, uint8_t draw_mode, uint8_t planes, uint8_t mask, uint8_t layer_mask, uint16_t src_line_pitch, uint8_t *bmp_data_src)
{
	uint32_t *dp = fb + (dy * fb_pitch);

	uint8_t cur_bit, base_bit, base_byte;
	uint16_t cur_byte = 0, u8_fg = 0;

	uint32_t plane_size = src_line_pitch * h;
	uint8_t *bmp_data = bmp_data_src;

  int16_t line_y, x;

	cur_bit = base_bit = (0x80 >> (sx % 8));
	cur_byte = base_byte = ((sx / 8) % src_line_pitch);

	for (line_y = 0; line_y < h; line_y++) {
		for (x = dx; x < dx + w; x++) {
			u8_fg = 0;
			if (draw_mode & 0x01) // If bit 1 is set, the inverted planar data is always used.
				DECODE_INVERTED_PLANAR_PIXEL(u8_fg)
			else
				DECODE_PLANAR_PIXEL(u8_fg)
			
			if (mask == 0xFF && (draw_mode == MINTERM_SRC || draw_mode == MINTERM_NOTSRC)) {
				((uint8_t *)dp)[x] = u8_fg;
				goto skip;
			}

			HANDLE_MINTERM_PIXEL_8(u8_fg, ((uint8_t *)dp)[x]);

			skip:;
			if ((cur_bit >>= 1) == 0) {
				cur_bit = 0x80;
				cur_byte++;
				cur_byte %= src_line_pitch;
			}

		}
		dp += fb_pitch;
		if ((line_y + sy + 1) % h)
			bmp_data += src_line_pitch;
		else
			bmp_data = bmp_data_src;
		cur_bit = base_bit;
		cur_byte = base_byte;
	}
}

void p2d_rect(uint32_t* fb, uint32_t fb_pitch, int16_t sx, int16_t sy, int16_t dx, int16_t dy, int16_t w, int16_t h, uint16_t sh, uint8_t draw_mode, uint8_t planes, uint8_t mask, uint8_t layer_mask, uint32_t color_mask, uint16_t src_line_pitch, uint8_t *bmp_data_src, uint32_t color_format)
{
	uint32_t *dp = fb + (dy * fb_pitch);

	uint8_t cur_bit, base_bit, base_byte;
	uint16_t cur_byte = 0, cur_pixel = 0;
	uint32_t fg_color = 0;

	uint32_t plane_size = src_line_pitch * h;
	uint32_t *bmp_pal = (uint32_t *)bmp_data_src;
	uint8_t *bmp_data = bmp_data_src + (256 * 4);

  int16_t line_y, x;

	cur_bit = base_bit = (0x80 >> (sx % 8));
	cur_byte = base_byte = ((sx / 8) % src_line_pitch);

	for (line_y = 0; line_y < h; line_y++) {
		for (x = dx; x < dx + w; x++) {
			cur_pixel = 0;
			if (draw_mode & 0x01)
				DECODE_INVERTED_PLANAR_PIXEL(cur_pixel)
			else
				DECODE_PLANAR_PIXEL(cur_pixel)
			fg_color = bmp_pal[cur_pixel];

			if (mask == 0xFF && (draw_mode == 0x0C || draw_mode == 0x03)) {
				switch (color_format) {
					case MNTVA_COLOR_16BIT565:
						((uint16_t *)dp)[x] = fg_color;
						break;
					case MNTVA_COLOR_32BIT:
						dp[x] = fg_color;
						break;
				}
				goto skip;
			}

			HANDLE_MINTERM_PIXEL_16_32(fg_color, dp);

			skip:;
			if ((cur_bit >>= 1) == 0) {
				cur_bit = 0x80;
				cur_byte++;
				cur_byte %= src_line_pitch;
			}

		}
		dp += fb_pitch;
		if ((line_y + sy + 1) % h)
			bmp_data += src_line_pitch;
		else
			bmp_data = bmp_data_src;
		cur_bit = base_bit;
		cur_byte = base_byte;
	}
}
